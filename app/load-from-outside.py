import sys, os
import instantPASDy

script_path = os.path.dirname(os.path.abspath( __file__ ))+'/'
working_dir = os.getcwd()

if __name__ == "__main__":

    if len(sys.argv) > 1:
        cfg = sys.argv.pop(1)
    else:
        cfg = script_path + '/examples/rover.cfg'

    instantPASDy.main(cfg)
