"""
CoRNy package
"""

from .version import __version__

# Date and Time
import pytz
from datetime import datetime, timedelta
import dateutil
from dateutil.relativedelta import *

# Data
import numpy as np
#np.warnings.filterwarnings('ignore')
import pandas as pd
import pandas
# Future Warning: mpl needs to be registered
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
pd.options.mode.chained_assignment = None

# System
import os
import sys
import io
import re
from glob import glob,glob1
import requests
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=UserWarning)

# Utilities
import configparser
from heapq import nsmallest, nlargest
from scipy import stats
from math import sin, cos, sqrt, atan2, radians
import itertools
from sklearn.neighbors import KDTree

# Output
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.backends.backend_pdf
import simplekml

# CoRNy
from .basics import *
from .io import *
from .figures import *
from .corn import *
from .config import *
import corny.exdata as exdata
from .exdata.openstreetmap import *
from .exdata.soilgrids import *
from .exdata.nmdb import *


# Overwrite print() with flush, i.e., no buffer
#_orig_print = print
#def print(*args, **kwargs):
#    _orig_print(*args, flush=True, **kwargs)
