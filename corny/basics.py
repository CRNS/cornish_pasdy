"""
CoRNy Basics
    General helper functions that make life easier
"""

from corny import *

import sys
import traceback
import numpy as np
import pandas
from glob import glob
name_std = '_std'
name_err = '_err'
from datetime import datetime
import pytz
import dateutil


def one_of(a, b, default=np.nan):
    """
    Return a or b or default.
    E.g.: latitude = one_of(lat_column, station_lat)
    """
    if a:
        return(a)
    elif b:
        return(b)
    else:
        return(default)

def safe_cast(var, to_type, default=None):
    """
    Convert variable type (int, float, str, ...) safely.
    Use:
    """
    if default is None:
         if   to_type==float: default = np.nan
         elif to_type==int:   default = np.nan
         elif to_type==str  : default = ''

    try:
        return(to_type(var))
    except (ValueError, TypeError):
        return(default)


def col_is_unique(s):
    """
    Check whether a pandas column has all identical values.
    Use: if col_is_unique(data.lon): data = data.iloc[0]
    """
    import numpy
    if isinstance(s, numpy.ndarray):
        a = s
    else:
        import shapely
        if isinstance(s[0], shapely.geometry.point.Point):
            a = np.array([pg.x for pg in s])
        else:
            import pandas
            if isinstance(s, pandas.Series):
                a = s.to_numpy()
            else:
                print('! col_is_unique: variable is neither of ndarray, pandas series, shapely point.')

    # Boolean array
    b = a[0] == a
    # Check all
    return(b.all())


def str2dt(s, tz=None):
    """
        str2dt('2021-12-24', tz='UTC')
        = datetime object
        Parses the date time string as a datetime object
    """
    dto = dateutil.parser.parse(s) # uses dateutil
    if tz is None:
        return(dto)
    elif tz == 'UTC':
        return(dto.replace(tzinfo=pytz.UTC))
    else:
        return(dto.replace(tzinfo=tz))

def interpret_time_format(s):
    s = s.replace('monthname', '%b').replace('month', '%M')
    s = s.replace('day', '%d').replace('hour', '%H').replace('year', '%Y')
    s = s.replace('minute', '%M').replace('min', '%M')
    return(s)

def time_resolution(data, unit='sec'):
    """
    Estimate time resolution of pandas in given units.
    """
    tres = data.index.to_series().dropna().diff().median().total_seconds()

    if   unit == 'min':  tres *= 60
    elif unit == 'hour': tres *= 60*60
    elif unit == 'day':  tres *= 60*60*24

    return(tres)


def clean_interval_str(a):
    """
    return valid interval string (1Min, 2H, ...) from user input
    """

    # if number is provided, assume it's in hours
    if a.isdigit(): a = str(a)+'hour'

    # harmonize spelling
    a = a.replace('sec','S').replace('hour','H').replace('min','Min').replace('day','D').replace('week','W').replace('month','M').replace('year','A')

    # remove plural s
    a = a.replace('s','')

    return(a)


def aggregate(data, aggstr, func='mean', set_std=[], adapt_err=[], min_count=None, verbose=False):
    """
        aggregate(data, '1H')
        = pandas
        Aggregation of the whole data set.
        Adds stddev columns for specific columns.
        Adapts error by 1/sqrt(a) for specific columns.
    """

    # Clean aggregation window string
    aggstr = clean_interval_str(aggstr)

    if verbose: print('| Aggregate data to ' + aggstr + ' time steps...', end='')

    # record time resolution
    tres_before = time_resolution(data)

    # Resample
    aggobj = data.resample(aggstr)
    if   func == 'mean':   data = aggobj.mean()
    elif func == 'median': data = aggobj.median()
    elif func == 'sum':    data = aggobj.sum(min_count=min_count)
    else:
        print('! Aggregation method unknown: ' + method)

    data = data.asfreq(aggstr)
    # record time resolution
    tres_after = time_resolution(data)

    # factor by which the time resolution is scaled up
    tres_factor = tres_after / tres_before
    tres_factor_err = 1 / np.sqrt( tres_factor )

    if verbose: print(' (data x%0.2f, error x%.02f)' % (tres_factor, tres_factor_err))

    # Errors scale with sqrt(1/tresfactor)
    for column in adapt_err:
        data[column + name_err] *= tres_factor_err

    # Add empirical standard deviation
    for column in set_std:
        data[column + name_std] = aggobj.std()[column]

    return(data)


def minly(data, agg=1, **kwargs):
    aggstr = '%iMin' % agg
    data = aggregate(data, aggstr, **kwargs)
    return(data)

def hourly(data, agg=1, **kwargs):
    aggstr = '%iH' % agg
    data = aggregate(data, aggstr, **kwargs)
    return(data)

def daily(data, agg=1, **kwargs):
    aggstr = '%iD' % agg
    data = aggregate(data, aggstr, **kwargs)
    data.index.freq = 'D'
    return(data)


# Moving average
def moving_avg(D, column, window=1, err=True, std=True, center=True):
    window = int(window)
    suffix = '_mov' #+str(window)
    rollobj = D[column].rolling(window, center=center, min_periods=3)
    D[column+suffix]                = rollobj.mean()
    if std: D[column+suffix+'_std'] = rollobj.std()
    if err: D[column+suffix+'_err'] = D[column+'_err'] * 1/np.sqrt(window)
    return(D)

def mavg(series, window=1, err=True, std=False, center=True):
    window = int(window)
    #suffix = '_mov' #+str(window)
    rollobj = series.rolling(window, center=center, min_periods=3)
    
    if std:
        r = rollobj.std()
        #D[column+suffix+'_std'] = rollobj.std()
    else:
        r = rollobj.mean()
    #if err: D[column+suffix+'_err'] = D[column+'_err'] * 1/np.sqrt(window)
    return(r)

"""
    cut_period(df, '2019-03-14', '2020-03-14')
    cut_period(df, datetime, datetime)
    cut_period(df, shorter_df)
    = df
    Cut period to that of another data frame.
"""
def cut_period_to(D, a, b=None):
    if a is None or a.strip() == '':
        a = D.index.min()
    if b is None or b.strip() == '':
        b = D.index.max()

    # Cut to other DataFrame
    if isinstance(a, pandas.DataFrame):
        return(D.loc[a.index.min():a.index.max()])

    # Strings to parse
    elif isinstance(a, str):
        if a.strip() == 'today':
            a = datetime.now().strftime('%Y-%m-%d')
            b = D.index.max().strftime('%Y-%m-%d %H:%M')

        a = str2dt(a, 'UTC')
        if isinstance(b, str):
            b = str2dt(b, 'UTC')
        print(a,b)
        return(D.loc[a:b])

    # Datetimes
    else:
        return(D.loc[a:b])


"""
    Performance(data[[a,b]]
    = [kge, nse, rmse, correl, var, bias]
    Evaluation of two columns of a df
"""
def Performance(data, title='Test', writeout=True):

    from hydroeval import evaluator, kge, rmse, nse

    columns = data.columns
    D = data.copy().dropna().values
    k = evaluator(kge,  D[:,0], D[:,1]).flatten()
    r = evaluator(rmse, D[:,0], D[:,1])
    n = evaluator(nse,  D[:,0], D[:,1])
    if writeout:
        print('%35s: KGE %.3f, NSE %.3f, RMSE %.3f, Correlation %.3f, Variance %.3f, Bias %.3f' %
            (columns[0]+' vs. '+columns[1], k[0], n[0], r[0], k[1], k[2], k[3]))
    else:
        return([k[0], n[0], r[0], k[1], k[2], k[3]])

def optimize(df, opt, cmp, span=(700,1100), func='kge'):
    if   func=='kge':
        fi = 0
        best = -1
    elif func=='nse':
        fi = 1
        best = -1
    elif func=='rmse':
        fi = 2
        best = 100000
    else: print('! Unknown function. Choose '+'kge, NSE, RMSE')

    best_i = 0
    for i in np.arange(span[0],span[1]):
        df[opt+'_opt'] = df[opt] * i
        p = Performance(df[[opt+'_opt', cmp]], writeout=False)
        if fi == 2:
            if p[fi] < best:
                best = p[fi]
                best_i = i
        else:
            if p[fi] > best:
                best = p[fi]
                best_i = i

    df[opt+'_opt'] = df[opt] * best_i
    print("%5i" % best_i, end='')
    Performance(df[[opt+'_opt', cmp]])


def ifdef(a, b, convert=float):
    if not a:
        return(b)
    else:
        return(convert(a))

def xsplit(s, range=0, type=str):
    """
    Split a string by , and ignore spaces,
    convert to certain data type,
    optionally generate an array of length `range`.
    E.g.: coords = xplit('51.5, 12.1, 120', range=3, type=float)
    """
    if isinstance(s, str) and s.strip() != '':
        #s = s.replace(',',' ')
        #a = np.array(s.split(), dtype=type)
        a = np.array([x.strip() for x in s.split(',')], dtype=type)
    else:
        a = np.array([])

    # fill up with NaNs
    for i in np.arange(range-len(a)):
        a = np.append(a, np.nan)
    return(a)

# Split a string by \s,\s into an array
def csplit(s, range=0, type=str):
    s = re.sub(r'\s*,+\s*', ',', s)
    s = re.sub(r',+', ',', s)
    a = np.array(s.split(','), dtype=type)
    return(a)

def chomp(x):
    if x.endswith("\r\n"): return x[:-2]
    if x.endswith("\n") or x.endswith("\r"): return x[:-1]
    return(x)



def read_files(pattern, index_col=1, skiprows=14):
    from glob import glob
    D = None
    for f in glob(pattern):
        Dx = pandas.read_csv(f, index_col=index_col, parse_dates=True, infer_datetime_format=True,
                             skipinitialspace=True, sep=',', skiprows=skiprows, encoding="cp850",
                       names=['RecordNum','t','#ClkTicks','PTB110_mb','P4_mb','P1_mb','Vbat','T1_C','RH1',
                              'N1Cts','N2Cts','N1ETsec','N2ETsec','N1T_C','N1RH','N2T_C','N2RH','T_CS215','RH_CS215',
                              'GpsUTC','LatDec','LongDec','Alt','Qual','NumSats','HDOP',
                              'Speed_kmh','COG','SpeedQuality','strDate'])
        if D is None:
            D = Dx.copy()
        else:
            D = pandas.concat([D, Dx])
    return(D)
    #D.head(10)

#######################################
#   Files in/out                      #
#######################################


def file_save_msg(file, name='File', end=''):
    size = os.stat(file).st_size
    if   size < 1024:    sizestr = "%.0f Bytes" % (size)
    elif size < 1024**2: sizestr = "%.0f KB"    % (size/1024)
    else:                sizestr = "%.1f MB"    % (size/1024/1024)
    print("< %s saved to %s (%s)" % (name, file, sizestr), end=end)


def ftp_is_file(FTP, object):
    """
    check if remote object is file
    """
    if FTP.nlst(object) == [object]:
        return True
    else:
        return False


def download(
        archive_file='',
        source='FTP',
        server=None,
        user=None,
        pswd=None,
        ftp_prefix='',
        ftp_suffix='',
        sftp=False,
        sd_path='',
        sd_prefix='',
        sd_suffix='',
        http_url='',
        http_filename='',
        update_all=False
        ):

    # if a folder was given
    if not archive_file.endswith('zip'):
        archive_file = os.path.join(archive_file,'') + 'archive.zip'

    # Check what has already been downloaded
    dirname = os.path.dirname(archive_file)
    if dirname != '' and not os.path.isdir(dirname):
        os.makedirs(dirname)
    archive = zipfile.ZipFile(archive_file, 'a', zipfile.ZIP_DEFLATED)
    archive_list = archive.namelist()
    archive_size = os.stat(archive_file).st_size /1024/1024
    print("i %5s files were already downloaded (%.2f MB)." % (len(archive_list), archive_size))

    local_files = pandas.DataFrame()
    local_files['name'] = archive_list
    local_files['size'] = [archive.getinfo(file).file_size for file in archive_list]

    if sftp:
        source = 'SFTP'

    if source=='FTP':
        # Select new files from FTP
        import ftplib
        try:
            remote = ftplib.FTP(server)
            print('| FTP connection OK', end='')
            remote.login(user, pswd)
            print(', login OK', end='')
            if '/' in ftp_prefix:
                folder = ftp_prefix[0:ftp_prefix.rindex('/')]
                ftp_prefix = ftp_prefix[ftp_prefix.rindex('/')+1:len(ftp_prefix)]
                remote.cwd(folder)
                print(', folder: '+folder, end='')

            print(', looking for: %s*' % ftp_prefix)
            remote_list = remote.nlst()
            remote_list = list(
                filter(
                    lambda item: item.startswith(ftp_prefix),
                    remote_list))
            print("i %5s files found on the FTP server " % len(remote_list))
            remote_files = pandas.DataFrame()
            remote_files['name'] = remote_list
            remote_files['size'] = np.nan
            #print('| Calculating size...', end='')
            # try: ftp.retrlines('LIST', lines.append)

            with Progressbar(len(remote_list), '| Calculating total size') as Pb:
                size_list = []
                for file in remote_list:
                    try:
                        remote_file_size = remote.size(file)
                        size_list.append(remote_file_size)
                        remote_files.loc[remote_files['name']==file, "size"] = remote_file_size
                    except:
                        # Probably not a file
                        pass
                    # size_list.append(remote.size(file))
                    Pb.update('(%.2f MB)' % (np.sum(size_list)/1024/2014))
            
            remote_files = remote_files.dropna(subset=["size"])
            # remote_files['size'] = size_list
            #[remote.size(file) for file in remote_list]
            #print('(%.2f MB)' % (remote_files['size'].sum()/1024/1024), flush=True)

        except ftplib.all_errors as e:
            print('')
            print('! FTP connection error: %s' % e)
            exit()

    elif source=='SFTP':

        from .exdata.sftp import SftpClient
        
        client = SftpClient(server, 22, user, pswd)
        print('i Looking for: %s*' % ftp_prefix)
        
        if '/' in ftp_prefix:
            folder = ftp_prefix[0:ftp_prefix.rindex('/')]
            ftp_prefix = ftp_prefix[ftp_prefix.rindex('/')+1:len(ftp_prefix)]
            
            remote_list = client.folder_list(folder)
            remote_list = list(filter(lambda item: item.startswith(ftp_prefix), remote_list))
            print("i %5s files found on the FTP server " % len(remote_list))
            remote_files = pandas.DataFrame()
            remote_files['name'] = remote_list
            remote_files['fullpath'] = [folder+"/"+x for x in remote_list]
            remote_files['size'] = np.nan

        with Progressbar(len(remote_list), '| Calculating file sizes') as Pb:
            size_list = []
            for file in remote_list:
                try:
                    remote_file_size = client.file_size(folder+"/"+file)
                    size_list.append(remote_file_size)
                    remote_files.loc[remote_files['name']==file, "size"] = remote_file_size
                except:
                    # Probably not a file
                    pass
                # size_list.append(remote.size(file))
                Pb.update('(%.2f MB)' % (np.sum(size_list)/1024/2014))

        remote_files = remote_files.dropna(subset=["size"])

    elif source=='SD':
        # Select new files from SD-Card
        #remote_list = list(filter(lambda item: item.endswith(suffix=''), os.listdir(sd_path)))
        remote_list = [os.path.basename(x) for x in glob(sd_path+'/'+sd_prefix+'*'+sd_suffix)]
        print("i %5s files found on the SD backup." % len(remote_list))
        remote_files = pandas.DataFrame()
        remote_files['name'] = remote_list
        remote_files['size'] = [os.stat(sd_path+'/'+file).st_size for file in remote_list]

    elif source=='HTTP':
        # Select new files from FTP
        from .exdata.http import download_http
        download_http(http_url, http_filename)
        remote_list = [http_filename]
        remote_files = pandas.DataFrame()
        remote_files['name'] = [http_filename]
        remote_files['size'] = [os.stat(http_filename).st_size]

    elif source=='DMP':
        # Select new files from DMP
        remote_list = [os.path.basename(x) for x in glob(sd_path+'/'+sd_prefix+'*'+sd_suffix)]
        print("i %5s files found on the SD backup." % len(remote_list))


    else:
        return

    if update_all:
        update_list = remote_list
    else:
        #update_list = list(filter(lambda item: not item in archive_list, remote_list))
        merged_files = pandas.merge(local_files, remote_files, how='right', on='name')
        new_files = merged_files[merged_files.size_x != merged_files.size_y]
        update_list = np.unique(new_files.name.values).tolist()
        #print(merged_files)
        #print(update_list)

    print("i %5s new files selected for download." % len(update_list))

    if len(update_list) == 0: return

    # Download files and add to Zip
    from io import BytesIO
    i = 0
    for filename in update_list:
        i += 1
        #print("|  Archiving files %5s/%s: %s" % (i, len(update_list), filename))
        progressbar(i-1, len(update_list), title='Archiving', prefix=filename)

        if source=='FTP':
            memory = BytesIO()
            remote.retrbinary("RETR " + filename, memory.write) # FTP->memory
            archive.writestr(filename, memory.getvalue())    # memory->archive

        if source=='SFTP':
            with BytesIO() as memory:
                # FTP->memory
                client.download(
                    folder+"/"+filename,
                    memory,
                    bytes=True
                    )
                # memory.seek(0)
                # memory->archive
                archive.writestr(filename, memory.getvalue())

        elif source=='HTTP':
            archive.write(filename, os.path.basename(filename))

        elif source=='SD':
            archive.write(os.path.join(sd_path,'') + filename, filename)

    progressbar(i-1, len(update_list), title='Archiving', prefix=filename, end=True)

    archive.close()
    if source=='FTP': remote.close()
    if source=='SFTP': client.close()
    file_save_msg(archive_file, "Archive", end="\n")



import re
re_dataselect = re.compile(r'//DataSelect\s*=\s*(\w+)')
re_columns = re.compile(r'/+(RecordNum.+)')
re_columns2 = re.compile(r'/+(GpsUTC.+)')
re_crnsdata = re.compile(r'^\d+')

def read(filename, archive=None, tz='UTC', path='', skip=0):

    header_skip = 0
    datastr = ''
    nodatastr = ''
    with archive.open(filename,'r') if archive else open(path+filename, encoding="cp850") as file:
        for _ in range(skip):
            next(file)

        for line in file:
            if isinstance(line, bytes):
                line = line.decode('cp850', errors='ignore').lstrip()
            if re_crnsdata.search(line):
                datastr += line
            else:
                nodatastr += line

    #with open('_debug_nodatastr.txt', 'a') as f:
    #    f.write(nodatastr)
    return(datastr+"\n")


def read_header(filename, archive=None, path=''):

    import re
    import os
    re_crnsdata = re.compile(r'^\d+\,')
    re_strangecoord = re.compile(r'\d,[EW],')
    re_columns = re.compile(r'/+(RecordNum.+)')
    re_columns2 = re.compile(r'/+(GpsUTC.+)')
    strange_coords = []
    data_columns = ''
    col_row_detected=False
    with archive.open(filename,'r') if archive else open(path+filename) as file:
        for line in file:
            if isinstance(line, bytes):
                line = line.decode(errors='ignore')

            if re_crnsdata.search(line):
                # get column number of strange direction
                strange_coords = [line.count(',', 0, m.start())+1 for m in re_strangecoord.finditer(line)]
                if strange_coords:
                    #print(line)
                    break
            else:
                match_columns = re_columns.search(line)
                if match_columns:
                    data_columns = match_columns.group(1)
                    col_row_detected=True
                    continue
                match_columns2 = re_columns2.search(line)
                if match_columns2:
                    data_columns += ',' + match_columns2.group(1)
                    continue

    if len(data_columns)<=0:
        print('! Input file contains no headers (e.g., //RecordNum...). Try to define input_columns in the config file.')
        return(None)
    else:
        data_columns = chomp(data_columns)
        data_columns = csplit(data_columns)
        #re.sub(r'\s*,+\s*', ',', data_columns)
        #data_columns = re.sub(r',+', ',', data_columns)
        #data_columns = data_columns.split(',')
        if strange_coords:
            print("\n!  "+'Found strange coordinate format and missing columns, trying to fix it...', end='')
            new_columns = ['NS','EW'] # Assuming that northing is first, easting is second.
            for pos in strange_coords:
                data_columns.insert(pos, new_columns.pop(0))
        #if the first row of col names was missing we manually add
        if not col_row_detected:
            print('Manually add half of column names')
            data_column_row=np.array(('RecordNum','Date Time(UTC)',
                                      'PTB110_mb','P4_mb','P1_mb','Vbat',
                                      'T1_C','RH1','N1Cts','N2Cts','N1ETsec',
                                      'N2ETsec','N1T(C)','N1RH','N2T(C)',
                                      'N2RH','T_CS215','RH_CS215'))
            #append together
            data_columns=np.append(data_column_row,data_columns[1:])
        return(data_columns)


def read_file(filename, archive=None, tz='UTC'):
    header_skip = 0
    datastr = ''
    with archive.open(filename,'r') if archive else open(filename, encoding="cp850") as file:
        for line in file:
            if isinstance(line, bytes):
                line = line.decode(errors='ignore')
            if re_crnsdata.search(line):
                datastr += line
    return(datastr+"\n")

import io
import os
import zipfile

def read_and_convert(path, prefix='', suffix='', data_columns=[], progress='file'):

    archive = None
    files = []

    if path.endswith('.zip'):
        archive = zipfile.ZipFile(path, 'r')
        files = [x for x in archive.namelist() if x.endswith(suffix) and x.startswith(prefix)]

    elif os.path.isdir(path):
        pattern = path + prefix +'*'+ suffix
        files = glob(pattern)
        if len(files)==0:
            print("ERROR: Folder is empty: " + pattern)
            return(None)
    else:
        print("ERROR: Input is neither a file, folder, nor a zip archive.")
        return(None)

    datastr = ''
    i = 0
    last_progress = 0 # in %
    i_max = len(files)

    for filename in files:
        i += 1

        if progress == 'percent':
            this_progress = (i+0.01)/len(files)//0.1*10 # in %, offset 0.01 because x/x//0.1 is 9
            if this_progress > last_progress:
                print("%.0f%% " % this_progress, end='')
                last_progress = this_progress

        elif progress == 'file':
            print('%5d/%d: %s' % (i, i_max, filename))

        if datastr=='':
            #if data_columns == []:
            #    data_columns = read_header(filename, archive)
            datastr = read_file(filename, archive)
        else:
            datastr += read_file(filename, archive) #data.append() #, verify_integrity=True)

    if not archive is None:
        archive.close()

    #DEBUG output raw data file
    #with open('temp.txt','a') as f:
    #    f.write(datastr)

    data = make_pandas(datastr, data_columns)
    if data is None:
        print('Error: Cannot interprete the data. Check the column header, is this correct? '+ ','.join(data_columns))
        return()
    else:
        return(data)

def make_pandas_old(datastr, file=False, columns=None, header_skip=0, index=1):
    if file:
        f = datastr
    else:
        f = io.StringIO(datastr)

    try:
        data = pandas.read_csv(f, index_col=index, parse_dates=True, infer_datetime_format=True, skipinitialspace=True,
                               sep=',', skiprows=12, encoding="cp850", on_bad_lines='skip', names=columns, dtype=object)
    except Exception as e:
        print("ERROR interpreting data format: "+str(e))
        return(None)

    data = data.apply(pandas.to_numeric, errors='coerce')
    return(data)
    
def make_pandas(datastr, columns=None, header_skip=0, index=1,
    sep=',', decimal='.', timestamp=False):

    with Progress('| Parsing data'):
        data = pandas.read_csv(io.StringIO(datastr), encoding='cp850', skipinitialspace=True,
                            sep=sep, skiprows=header_skip, names=columns, decimal=decimal,
                            on_bad_lines='skip',
        # Depreciated: no longer convert columns directly on input:
        # parse_dates=True, infer_datetime_format=True, index_col=index, dtype = dtypes
        # dtypes = { c : float for c in columns }; dtypes[columns[index]] = str
                            dtype=object)
    
    with Progress('| Tidy up'):
        # Tidy up
        # Remove NaN values
        data = data.loc[data.index.dropna()]
        # Remove duplicates
        data = data[~data.index.duplicated(keep='first')]
        len1 = len(data)

        ## Convert time column to a valid DateTime object
        if isinstance(index, list):
            if len(index) == 2:
                index_column = '_time'
                #TODO: check if index out of bounds
                data[index_column] = pandas.to_datetime(data.iloc[:,index[0]] +' '+ data.iloc[:,index[1]], errors='coerce')
            else:
                print(index)
        else:
            index_column = columns[index]
            if timestamp:
                data[index_column] = pandas.to_datetime(data[columns[index]], errors='coerce', unit='s')
            else:
                data[index_column] = pandas.to_datetime(data[columns[index]], errors='coerce')

        #print(data.head(3))
        ## drop any failures
        data = data.dropna(subset=[index_column])
        ## Set the DateTime column as index
        data.set_index(index_column, inplace=True)
        
        # Cases when decimal is not .
        decimal = decimal.strip()
        if decimal != '.':
            data = data.apply(lambda x: x.str.replace(decimal,'.'))
        ## Convert all the regular columns to numeric and drop any failures
        data = data.apply(pandas.to_numeric, errors='coerce')

        ## Sort and unify the index 
        data = data.sort_index().drop_duplicates()
        # and set the time zone as UTC
        if data.index.tzinfo is None: # or d.tzinfo.utcoffset(d) is None
            data = data.tz_localize('UTC')
        else:
            data = data.tz_convert('UTC')
        # remove dates in the future
        data = data[data.index <= datetime.now(tz=pytz.UTC)]

        #print(data.head(3))
        #print(data.index.dtype)
        
        ## Find out which column names are null
        bad_columns = []
        for c in range(len(columns)):
            if pandas.isnull(columns[c]):
                bad_columns.append(c)
        ## Reassign data using only non-bad columns
        data = data.iloc[:, [i for i,n in enumerate(data.columns) if i not in bad_columns]]

        #print(data.head(3))

    len2 = len(data)
    if len2 < len1: print("i   Dropped %i malformed lines." % (len1-len2))
    
    print(data.head(3))

    return(data)
    

def check_data(data):
    """
    Quickly check whether the DataFrame is empty.
    """
    if data is None:
        print('! Invalid data object, maybe check headers and period.')
    elif len(data)==0:
        print('! No remaining records in dataset. Check variable ranges, bbox, period.')
    else:
        return(True)
    return(False)


def read_corny(file):
    data = pandas.read_csv(file, index_col=0, parse_dates=True, infer_datetime_format=True, skipinitialspace=True,
                           sep=',', encoding="cp850", on_bad_lines='skip') #, dtype=object)
    print("%i lines, %i columns: %s" % (len(data), len(data.columns), ', '.join(data.columns)))
    return(data)

read_cornish = read_corny

def read_geojson(file, columns=['label','short','type','lat','lon']):
    import geojson
    with open(file) as f:
        gj = geojson.load(f)

    # Run through geojson and fill a Sensor DataFrame
    D = pandas.DataFrame([], columns=columns)
    for i in range(len(gj['features'])):
        Dx = pandas.DataFrame([[
            gj['features'][i]["properties"]['Id'],
            gj['features'][i]["properties"]['short'],
            gj['features'][i]["properties"]['Inst'],
            gj['features'][i]["geometry"]["coordinates"][1],
            gj['features'][i]["geometry"]["coordinates"][0]]],
            columns=columns)
        D = D.append(Dx)
    D = D.sort_values('label')
    D['color'] = 'black'
    D = D.set_index(np.arange(1,len(D)+1))
    return(D)



def report_size(file, verb='Saved', unit='MB'):
    import os
    if unit=='KB':
        factor = 1/1024
    elif unit=='MB':
        factor = 1/1024/1024
    else:
        factor = 1
        unit = 'unknown'
    size = os.stat(file).st_size * factor
    print("%s %s (%.1f %s)." % (verb, file, size, unit))

# Spatial calculations

def latlon2utm(lats, lons, epsg=31468):
    import pyproj
    if isinstance(lats, pandas.DataFrame) or isinstance(lats, pandas.Series):
        lats = lats.values
        lons = lons.values

    ## Deprecated:
    # import pyproj
    # utmy, utmx = pyproj.transform(pyproj.Proj("epsg:4326"), pyproj.Proj("epsg:31468"), lats, lons)
    ## New pyproj2 syntax:
    from pyproj import Transformer
    utmy, utmx = Transformer.from_crs("epsg:4326", "epsg:%s" % str(epsg)).transform(lats, lons)

    return(utmy, utmx)

def utm2latlon(utmx, utmy, epsg=31468):
    import pyproj
    if isinstance(utmx, pandas.DataFrame) or isinstance(utmx, pandas.Series):
        utmx = utmx.values
        utmy = utmy.values

    ## Deprecated:
    # import pyproj
    # lats, lons = pyproj.transform(pyproj.Proj("epsg:31468"), pyproj.Proj("epsg:4326"), utmy, utmx)
    ## New pyproj2 syntax:
    from pyproj import Transformer
    lats, lons = Transformer.from_crs("epsg:%s" % str(epsg), "epsg:4326").transform(utmy, utmx)

    return(lats, lons)


def fineround(x, reso=3, fine=1):
    # A rounding function that can round to values even in between digits
    # e.g. use fine=1 to round to the nearest .0 (standard digit round)
    #          fine=2 to round to the nearest .0 or .5
    #          ...

    return(np.round(x*fine, int(reso))/fine)


#######################################
#   Spatial Geoanalysis               #
#######################################


#JJ recalculate strange coordinates to proper decimals
# argument is a pandas.DataFrame(columns=[number, direction])
# remember that C.iloc[:,i] is the i^th column of C

def deg100min2dec(C):
    # Convert
    x = C.iloc[:,0] / 100
    deg = np.floor(x)
    dec = deg + (x - deg)/0.6
    # Negate if cardinal direction is south or west
    dec *= C.iloc[:,1].map({'N': 1, 'S': -1, 'E': 1, 'W': -1})
    return(dec)


#JJ use x nearest neighbors for averaging
## find k closest lat/lon to every lat/lon coordinates
def distance(pointA, pointB):
    return sqrt((pointB[0] - pointA[0]) ** 2 + (pointB[1] - pointA[1]) ** 2)

import zipfile
import os


def distanceinkm(xstand,ystand,xfor,yfor): # approximate radius of earth in km

    R = 6373.0
    lat1 = radians(xstand)
    lon1 = radians(ystand)
    lat2 = radians(xfor)
    lon2 = radians(yfor)

    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = R * c
    return(distance)

def Neighbours(lat, lon, numpoints, data):
    numpoints = numpoints+1 # points itself included - therefore add one to get desired output
    points = np.vstack((lat, lon)).T # create numpy array from coordinates

    kdt = KDTree(points, leaf_size=30, metric='euclidean') # build tree with euclidean distances
    neighborpoints = kdt.query(points, k=numpoints, return_distance=False) # find the closest k points

    # calculate maximal distance within the k clostest coordinates
    neighbordist = []
    for i in range(len(points)):
        furthest = np.array(nlargest(1, points[neighborpoints[i]], lambda p : distance(p, (points[i])))) # find the coordinates for the k closest points
        neighbordist.append(distanceinkm(points[i][0],points[i][1], furthest[0][0],furthest[0][1])*1000)

    return neighbordist, neighborpoints

    #neighborpoints = [] # create empty list for all results

    #for i in range(len(points)): ## iterate for all points
    #    nearest = np.array(nsmallest(numpoints, points, lambda p : distance(p, (points[i])))) # find the coordinates for the k closest points
    #    print(nearest)
    #    index = [] # create empty list for the index of the positions

    #    for m in range(len(nearest)):
    #        idx, rest = np.where(points==nearest[m]) # find the rows of these closest k coordinates
    #        idx = stats.mode(idx) # calculate the mode - necessary because sometimes several outputs from above
    #        index.append(idx.mode.tolist()) # create list from the k rows found

    #    neighborpoints.append(index) # create list of k closest locations for all locations
    #for i in range(len(neighborpoints)):
    #    neighborpoints[i] = list(itertools.chain(*neighborpoints[i])) # reduce for one nested list


#######################################
#   Console                           #
#######################################


def report(data, column, meanof=None, correction='', units='', format='i %s'):
    if meanof is None: meanof = column
    evallist = [column] + [data[c].mean() for c in meanof] + [units]
    s = format % tuple(evallist)
    if correction != '':
        if isinstance(correction, list):
            q = 1
            for c in correction:
                q *= data[c]
        elif correction in data.columns:
            q = data[correction]
        s += ' (%+.0f%%)' % (100*(1-q.mean()))
    print(s)

def report_mmsm(data):
    print("%15s %.2f ... %.2f +/- %.2f ... %.2f" % (data.name, data.min(), data.mean(), data.std(), data.max()))

def cprintf(s, fmt=''):
    os.system("") # makes it work also in the Win10 Console
    a = ''
    if fmt=='black':     a='\033[30m'
    if fmt=='red':       a='\033[31m'
    if fmt=='green':     a='\033[32m'
    if fmt=='yellow':    a='\033[33m'
    if fmt=='blue':      a='\033[34m'
    if fmt=='magenta':   a='\033[35m'
    if fmt=='cyan':      a='\033[36m'
    if fmt=='white':     a='\033[37m'
    if fmt=='underline': a='\033[4m'
    return(a + s + '\033[0m')


class Progress():
    """
    Progress environment
    Why?
        No more cluttering of print statements before and after code
        
    Usage:
        with Progress(text, format) as Pr:
            pass
            
        Example #1:
            with Progress('Calculating'):
                x = 1+1
            # Returns: | Calculating ... OK.
        
        Example #1:
            a, b = 3, 4
            with Progress('Calculating', a, '+', b) as Pr:
                x = a+b
                Pr.ok = ' = %d' % x
            # Returns: | Calculating 3 + 4 ... 5
        
        Example #2:
            with Progress('Reading file', fmt='R') as Pr:
                try:
                    open(file, 'r')
                except:
                    Pr.fail()
            # Returns: > Reading file ... FAILED!
        
        Example #3:
            with Progress('Running', replace=True) as Pr:
                Pr.update("Step 1")
                ...
                Pr.update("Step 2")
                ...
            # Returns: | Running... Step 1
            #  (replaces line with Step 2, ...)
    """
    use_colors = False

    def __init__(self, *args, ok='OK.', use_colors=None, sep='...', replace=False):
        self._fail = False
        self.args = args
        self.ok = ok
        self.sep = sep
        self.startstr = ''.join(*self.args)+self.sep
        self.replace = replace
        self.longest_line = 0

        if use_colors is None:
            self.use_colors = Progress.use_colors
        else:
            self.use_colors = use_colors
        self.colorize_continue = None

    def print_status(self, s, end="\n"):
        if self.use_colors:
            s = self.colorize(s, end=end)
        if self.replace:
            end = "\r" 
            if s != self.startstr:
                s = self.startstr + s
            length = len(s)
            if length > self.longest_line:
                self.longest_line = length
            else:
                s += " " * (self.longest_line - length)
            
        print(s, end=end, flush=True)

    def colorize(self, s: str, end=None):
        color_start = self.colorize_continue
        if s[0:2] == '> ': color_start = '\033[33m'
        if s[0:2] == '< ': color_start = '\033[32m'
        if s[0:2] == '# ': color_start = '\033[35m'
        if s[0:2] == 'i ': color_start = '\033[36m'
        if s[0:2] == '! ': color_start = '\033[31m'
        if 'Error:' in s or 'FAILED' in s:
            color_start = '\033[31m'
        if color_start:
            s = color_start + s + '\033[0m'
        if end == '':
            self.colorize_continue = color_start
        else:
            self.colorize_continue = None
        return(s)

    def update(self, s):
        if not self.replace:
            s = s + self.sep
        self.print_status(s, end='')

    def fail(self):
        self._fail = True
        self.print_status('FAILED!')

    def __enter__(self):
        self.print_status(self.startstr, end='')
        return(self)

    def __exit__(self, except_type, except_value, tb):
        if not except_type:
            self.print_status(self.ok)
        else:
            self.print_status('FAILED!')
            self.print_status('!'+str(except_value))
            traceback.print_exc()
        return(True)

class Progressbar():
    """
    Progressbar environment
    Usage:
        with Progressbar(size, title, length) as Pb:
            Pb.update(text, index)
            
    Example 1:
        L = ['a','b','c','d','e']
        with Progressbar(L) as Pb:
            for l in L:
                Pb.update()
        # Returns: | Please wait >>>>>>>               25%

    Example 2:
        files = glob('*.txt')
        with Progressbar(len(files), 'Reading') as Pb:
            i = 0
            for file in files:
                Pb.update(file, i)
                fh = open(file, 'r')
                i += 1
    # Returns: | Reading    >>>>>>>               25% readme.txt

    # Alternatives:
        from tqdm import tqdm
        with tqdm(L) as Pb:
            for l in L:
                Pb.update()
    """

    def __init__(self, total, title='Please wait', length=20):
        
        # Interpret total number of elements
        if isinstance(total, int):
            self.total = total
        elif isinstance(total, float) or isinstance(total, str):
            try:
                self.total = int(total)
            except:
                print('! Progressbar cannot understand total bar length: ', total)
        elif isinstance(total, list):
            self.total = len(total)
        else:
            try:
                self.total = len(list(total))
            except:
                print('! Progressbar cannot understand total bar length: ', total)

        # Title
        self.title = title

        # Terminal length
        if length is None:
            import shutil
            self.length = shutil.get_terminal_size()[0]-79
        else:
            self.length = length

        self.comment = ''

    def update(self, comment='', progress=None):
        if progress is None:
            self.progress += 1
        else:
            self.progress = progress
        self.comment = comment
        self.printstr()

    def printstr(self):
        self.progress_pc = round((self.progress)/self.total*100, 1)
        progress_str = '>' * int(round(self.progress_pc/(100/self.length)))
        remaining_str = ' ' * int(self.length - len(progress_str))
        print('%-11s %s%s %3.0f%% %s' % (self.title, progress_str, remaining_str, self.progress_pc, self.comment), end='\r')
        sys.stdout.flush()

    def __enter__(self):
        self.update('',0)
        return(self)

    def __exit__(self, except_type, except_value, tb):
        if not except_type:
            self.update(' '*60 +"\n", self.total)
        else:
            print('')
            print('!', except_value)
            traceback.print_exc()
        return(True)


# Depreciated
def progressbar(index, total, length=None, title='Please wait', prefix='', end=False):
    import shutil
    if length is None:
        length = shutil.get_terminal_size()[0]*0.5 #-99
    percent_done = round((index+1)/total*100, 1)
    done = round(percent_done/(100/length))
    togo = length-done

    done_str = '>' * int(done) # '�'
    togo_str = ' ' * int(togo)

    if end: prefix = ' '*60

    print('| %-11s %s%s %3.0f%% %s' % (title, done_str, togo_str, percent_done, prefix), end='\r')
    sys.stdout.flush()

    #if end: print('')
