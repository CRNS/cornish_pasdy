"""
CoRNy Config
    Deal with Configuration files
"""

#from corny import *
import os
import configparser
from corny.basics import Progress, safe_cast, xsplit, str2dt
from corny.io import format_path, make_dir

class Config():
    def __init__(self, file=None, chdir=True,
                format_path = [('input','path'),('output','out_path')],
                make_dir = [('output','out_path')]):
        if file is None: file = 'config.cfg'
        self.file = file
        self._chdir = chdir
        self._format_path = format_path
        self._make_dir = make_dir
        self.config = None

    def read(self):
        self.config = read_config(self.file)

        if self.config:
            if self._chdir:
                config_dir, config_file_name = os.path.split(self.file)
                os.chdir(os.path.normpath(config_dir))

            if self._make_dir:
                for sec, key in self._make_dir:
                    make_dir(self.get(sec, key))

            if self._format_path:
                for sec, key in self._format_path:
                    self.config[sec][key] = format_path(self.get(sec, key))

        return(self)

    def check(self, sec, key, value=None):
        return(cc(self.config, sec, key, value))

    def get(self, sec, key, alt=None, dtype=None, range=None, types='str'):
        return(gc(self.config, sec, key, alt, dtype, range, types))

    def str(   self,    sec, key, alt=None):
        return(self.get(sec, key, alt, dtype=str))
    def int(   self,    sec, key, alt=None):
        return(self.get(sec, key, alt, dtype=int))
    def float( self,    sec, key, alt=None):
        return(self.get(sec, key, alt, dtype=float))
    def bool(  self,    sec, key, alt=None):
        return(self.get(sec, key, alt, dtype=bool))
    def list(  self,    sec, key, alt=None, range=None, types='str'):
        return(self.get(sec, key, alt, dtype=list, range=range, types=types))
    def date(  self,    sec, key, alt=None):
        return(self.get(sec, key, alt, dtype='date'))
    def multi( self,    sec, key, alt=None):
        return(self.get(sec, key, alt, dtype='multi'))



def read_config(file, chdir=False):
    # Config
    config = configparser.ConfigParser(interpolation=None)
    if file is None: file = 'config.cfg'
    """ Deprecated
    if not os.path.isfile(file):
        print(cprintf('! Config file required! Either specify it as an argument or provide ./config.cfg', 'red'))
        return()
    else:
        print("i Working with %s" % file)

    temp = config.read(file)
    """
    with Progress('> Loading config: %s' % file) as Pr:
        if not os.path.isfile(file):
            Pr.fail()
            print('! Config file required! Either specify it as an argument or provide config.cfg.')
            return()
        else:
            temp = config.read(file)
    
    if chdir:
        # Change to this directory
        config_dir, config_file_name = os.path.split(file)
        os.chdir(os.path.normpath(config_dir))

    # clean config
    if os.path.isdir(config['input']['path']):
        # add trailing slash
        config['input']['path'] = os.path.join(config['input']['path'], '')
    # check whether output path exists
    """ Deprecated
    if config['output']['out_path']:
        if not os.path.exists(config['output']['out_path']):
            os.makedirs(config['output']['out_path'])
    """
    output_path = gc(config,'output','out_path',alt='output')
    if not os.path.exists(output_path):
        with Progress('< Creating folder %s' % output_path):
            os.makedirs(output_path)

    # add trailing slash
    config['output']['out_path'] = os.path.join(config['output']['out_path'], '')

    return(config)

def check_config(config, section, key, value=None):
    # return False if item does not exist or is empty
    r = False
    if section in config:
        if key in config[section]:
            provided_value = config[section][key].strip()
            if value is None:
                if provided_value != '' and provided_value.lower() != "none":
                    r = True
            elif provided_value == value:
                r = True
    return(r)

cc = check_config

def get_config(config, section, key, alt=None, dtype=None, range=0, types=str):
    # return None if item does not exist or is empty
    r = alt
    if section in config:
        if key in config[section]:
            if config[section][key].strip() != '':
                r = config[section][key].strip()

    if not r is None:
        if not dtype is None:
            if dtype=='multi':
                r = [item for item in r.split('\n')]
            elif dtype=='date':
                r = str2dt(r)
            elif dtype==list:
                r = xsplit(r, range=range, type=types)
                if len(r)>0:
                    pass
                else:
                    r = []
            elif dtype==bool:
                r = yesno2bool(r)
            else:
                r = safe_cast(r, dtype)
    return(r)

gc = get_config

def yesno2bool(s):
    if isinstance(s, str):
        s = s.lower().strip()
        if s == 'yes' or s == 'y' or s=='true' or s=='1':
            return(True)
        else:
            return(False)
    else:
        return(False)
