"""
CoRNy Conversion
================
Conversion functions neutrons <-> water
"""

# Note this file is not used/active yet

from numpy import nan, isscalar
from importlib import reload

# from basics import list_to_str, slice_dict, ensure_numeric, check_required_args
#reload(basics)
# import .corrections
# import .units as un
# from .grains.Desilets2010wrr import sm_to_N as sm_to_N_Desilets
# from .grains.Koehli2021fiw   import sm_to_N as sm_to_N_Koehli

# %%
def clay_to_lw(clay, method='Greasen_etal_1985'):
    if method == 'Franz_etal_2012':
        from grains.Greasen1985x import clay_to_lw
        lw = clay_to_lw(clay)
    else:
        print('! Unknown method to convert Clay to lattice water.')
        lw = nan        
    return(lw)
    
def corg_to_owe(corg, method='Franz_etal_2012'):
    if method == 'Franz_etal_2012':
        from grains.Franz2015vzj import corg_to_owe
        owe = corg_to_owe(corg)
    else:
        print('! Unknown method to convert C_org to organic water equivalent.')
        owe = nan        
    return(owe)
    
# %%
def sm_to_N(sm, method='Desilets_etal_2010', from_vol=False, 
    err=None, err_low=None, err_upp=None, return_asym_errors=False,
    correct_lw=None, correct_owe=None, **kwargs):
    """
    Wrapper for various methods to convert SoilMoisture to Neutrons.
    1. Optional: convert volumetric to gravimettric
    2. Makes sure to forward scalar, array, or pdseries (using nans, no lists, ...)
    3. Optional: corrects sm = corrections.correct(sm, x, y) for x,y in zip(correct.keys, correct.values) . But it is cleaner to do that separately.
    4. Calls conversion grains.[method].sm_to_N(sm: grv, h, params) -> scalar | array
    5. Optional: Error propagation by repeated calls to sm_to_N(x) for x in errors, returns list of arrays [N, N_err]. Use with df['sm'], df['sm_err'] = a, b
    6. Makes sure to return numerical scalar or array
    """    
    # ensure numerical values (not lists, insert nans, ...)
    sm = ensure_numeric(sm)

    if from_vol:
        # Convert volumetric to gravimetric
        if check_required_args('vol_to_grv', ['bd'], kwargs):
            sm = un.vol_to_grv(sm, bd=ensure_numeric(kwargs['bd']))
        else:
            return(nan)

    if correct_lw:
        sm = corrections.correct_lw_r(sm, ensure_numeric(correct_lw))
    if correct_owe:
        sm = corrections.correct_owe_r(sm, ensure_numeric(correct_owe))
        
    # initialize
    N = nan
    r = []
    convert = None

    # Reference conversion functions
    if method == 'Desilets_etal_2010':
        if check_required_args(method, ['N0'], kwargs):
            convert = lambda x: sm_to_N_Desilets(x,
                N0 = ensure_numeric(kwargs['N0']),
                **slice_dict(['params'], kwargs))
            
    elif method == 'Koehli_etal_2021':
        if check_required_args(method, ['N0', 'h'], kwargs):
            convert = lambda x: sm_to_N_Koehli(x,
                N0 = ensure_numeric(kwargs['N0']),
                h  = ensure_numeric(kwargs['h']),
                **slice_dict(['params'], kwargs))
    
    # Leave if no conversion can be safely called
    if convert is None: return(nan)
        
    # Convert value
    N = convert(sm)
    r.append(N)    

    # Convert errors
    if err or err_low or err_upp:
        # Use err_low or err_upp if provided, fallback to err
        sm_err_low = ensure_numeric(err_low) if err_low else ensure_numeric(err)
        sm_err_upp = ensure_numeric(err_upp) if err_upp else ensure_numeric(err)
        # Propagate
        N_err_low = convert(sm + np.abs(sm_err_upp))
        N_err_upp = convert(sm - np.abs(sm_err_low))
        # Return both or mean
        if return_asym_errors:
            # return both deviations from N
            r.append(N_err_low - N)
            r.append(N_err_upp - N)
        else:
            # return mean deviation from N
            r.append(np.nanmean([N_err_upp-N, N-N_err_low], axis=0))

    return(r)

# %%
"""
import numpy as np
n, nerr1, nerr2 = sm_to_N([0.1,0.55,0.3], h=10, err=[0.01, 0.05, 0.02],
    method='Koehli_etal_2021', N0=1000, return_asym_errors=True)
n
nerr2
"""
# %%

"""
COSMOS.sm_to_N() -> Neutrons

    collects tagged sm, h, bd
    From the returned Neutrons, extract also errors and write to data
    calls SoilMoisture.sm_to_N(sm, h, bd, method, params) -> Neutrons

        Makes sure that units are correct in grv., same for AirHumidity. 
        Tryies to look for ah, bd (tagged) data in parent if not provided.
        Converts SoilMoisture, Bd, and AirHumidity to np.array()
        Correct for parent.lw, parent.org or warn if not tagged as already corrected.
        Automatically sets errors=[.err, .err_low, .err_upp]
        From the returned Neutrons, extract also errors and write to parent.data
        Calls conversion.sm_to_N(sm: grv, h, bd, method, params,
            from_vol=False, errors=[sm_err_low, sm_err_upp, sm_err],
            correct=dict(lw=[], org=[])) -> scalar | array | list

            Should take scalar, array, or pdseries using pandas.to_numeric(sm) and .values if isinstance pandas.Series
            Optional: convert vol to grv
            Optional: error propagation calls sm_to_N(x) for x in errors, returns list of arrays [N, N_err]. Use with df['sm'], df['sm_err'] = a, b
            Optional: calls sm = corrections.correct(sm, x, y) for x,y in zip(correct.keys, correct.values) . But it is cleaner to do that separately.
            Calls grains.Koehli2021fiw.sm_to_N(sm: grv, h, params) -> scalar | array
                
                Calculates N, does not check on units or types
            
"""
