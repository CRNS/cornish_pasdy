"""
CoRNy package
    External data
"""

__all__ = ["openstreetmap", "soilgrids","meteoservice","nmdb","raster","corine"]