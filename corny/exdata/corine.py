"""
Call function for roverweb backend landcover
"""

from corny import *

from .roverweb import landcover
import geopandas as gpd
from copy import deepcopy

def retrieve_corine_data(data, lon='LongDec',lat='LatDec'):
    """


    Parameters
    ----------
    data : TYPE
        DESCRIPTION.
    lon : TYPE, optional
        DESCRIPTION. The default is 'LongDec'.
    lat : TYPE, optional
        DESCRIPTION. The default is 'LatDec'.

    Returns
    -------
    None.

    """
    #if lon and lat is a float, we add this as extra columns
    if isinstance(lat,float) and isinstance(lon,float):
        data['LatDec']=deepcopy(lat)
        data['LongDec']=deepcopy(lon)
        lat='LatDec'
        lon='LongDec'
    #remove rows with no geographic coordinate
    data = data[data[lon].notna()]
    data = data[data[lon].notna()]

    #create geodataframe
    data=gpd.GeoDataFrame(data,geometry=gpd.points_from_xy(data[lon],data[lat]),crs='epsg:4326')
    #remember all old columns
    col_nm_old=data.columns.values.tolist()
    #call the main functionality in roverweb
    data=landcover.get_wfs_corine_germany(data)
    #get the subset of all retrieved layers
    new_columns=list(set(data.columns.values.tolist()) - set(col_nm_old))
    corine_data=data[new_columns].copy()

    print('Retrieval and processing landcover from corine successful')

    return corine_data
    
