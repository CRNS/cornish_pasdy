
import requests
import re
import os


def file_save_msg(file, name='File', end=''):
    size = os.stat(file).st_size
    if   size < 1024:    sizestr = "%.0f Bytes" % (size)
    elif size < 1024**2: sizestr = "%.0f KB"    % (size/1024)
    else:                sizestr = "%.1f MB"    % (size/1024/1024)
    print("< %s saved to %s (%s)" % (name, file, sizestr), end=end)


def make_url(id):
    url = "http://www.somedataserver.com/csvdata.php?ID=%s" \
        % (id)
    return(url)

def download_http(
        url,
        file,
        head_re = r'^\#',
        data_re = r'^\d',
        verbose = True
        ):
    """
    Download data from HTTP website

    Args:
        url (str): URL of the website
        file (str): filename or None (then return)
    
    Usage:
        id = 999
        file = "data/%0.f.csv" % id
        url = make_url(id)
        download_http(url, file)
    """
    
    if verbose: print('> Requesting URL: ' + url)
        
    try:
        r = requests.get(url)
    except Exception as e:
        print('! Requesting HTTP data failed.')
        print(e)
        r = None

    if r:
        if verbose: print('i Received %s characters.' % len(r.text))
        
        # Write into file
        re_head_line = re.compile(head_re)
        re_data_line = re.compile(data_re)

        with open(file, 'w') as f:
            for line in r.text.splitlines():
                if re_head_line.search(line):
                    f.write(line + "\n")
                elif re_data_line.search(line):
                    f.write(line + "\n")
        
        if verbose: file_save_msg(file, end="\n")
