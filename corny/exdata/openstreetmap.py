"""
CoRNy exdata.OpenStreetMap
    Get Data from OpenStreetMap
"""

import numpy as np
from corny import *
import pandas
from shapely.geometry import Point,Polygon


class OsmGraph:

    road_colors = dict(
        footway = "C7",
        mixed = "black",
        path = "C2",
        primary = "C3",
        service = "C8",
        track = "C5",
        trunk = "C4",
        trunk_link = "C6",
        unclassified = "#CCCCCC",
        motorway = "red",
        motorway_link = "pink",
        residential = "C9",
        secondary = "C1",
        tertiary = "C0",
        road = "#888888",
        primary_link = "violet",
        secondary_link = "plum",
        tertiary_link = "thistle",
        none = "white"
        )
    
    def __init__(self, network="rivers", bbox=None):

        self.network = "none"
        if network.startswith("river"):
            self.network = "rivers"
        elif network.startswith("road"):
            self.network = "roads"

        self.bbox = bbox
        self.graph = None
        self.data = None
        self.road_colors = OsmGraph.road_colors

    def download_graph(self, bbox=None, **kwargs):
        """
        Download a network Graph
        """
        import osmnx as ox
        if bbox is None:
            bbox = self.bbox
        try:
            self.graph = ox.graph_from_bbox(
                bbox[3], bbox[2], bbox[0], bbox[1],
                **kwargs)
        except:
            self.graph = None
            print("! OsmGraph: could not find a graph for network %s." % self.network)
            
        return(self)

    def to_data(self):
        """
        Convert a graph to a data
        """
        if self.graph is None:
            self.data = None
            # print("! OsmGraph: missing graph, use download_graph() first.")
            return(self)
        
        import osmnx as ox
        try:
            self.data = ox.graph_to_gdfs(
                self.graph,
                nodes=False, edges=True,
                fill_edge_geometry=True)
        except:
            self.data = None
            # print("! OsmGraph: cannot convert network graph of %s." % self.network)

        return(self)

    def get(self):
        """
        Wrapper to generate river or road data
        """
        if self.network == "rivers":
            self.get_rivers()

        elif self.network == "roads":
            self.get_roads()

        return(self)

    def get_rivers(self):
        """
        Get data of network graph of rivers
        """
        self.download_graph(
            self.bbox,
            simplify         = False,
            truncate_by_edge = True,
            retain_all       = True, 
            custom_filter    = '["waterway"~"river"]'
            )
        self.to_data()

        return(self)

    def get_roads(self):
        """
        Get data of network graph of roads
        """
        self.download_graph(
            self.bbox,
            simplify         = True,
            truncate_by_edge = True,
            retain_all       = True
            )
        self.to_data()

        if not self.data is None:
            self.data["highway_str"] = ""
            for i, row in self.data.iterrows():
                row = row.copy()
                if isinstance(row["highway"], list):
                    if 'primary' in row["highway"]:
                        self.data.loc[i, 'highway_str'] = 'primary'
                    elif 'track' in row["highway"]:
                        self.data.loc[i, 'highway_str'] = 'track'
                    else:
                        self.data.loc[i, 'highway_str'] = 'mixed'
                else:
                    self.data.loc[i, 'highway_str'] = row["highway"]

            # Unique list of road categories
            self.road_categories = np.unique(self.data["highway_str"].values)
            # Set every unknown road category to none
            self.road_categories = [r if r in OsmGraph.road_colors else "none" for r in self.road_categories]

        return(self)

    def plot(self, ax):
        """
        Plotting routine for OsmGraphs. Expects ax.
        """
        if self.data is None:
            print("! OsmGraph %s: Nothing to plot." % self.network)
        else:
            plot_kw = dict()
            if self.network == "rivers":
                plot_kw = dict(
                    zorder=1,
                    color="blue", alpha=0.1,
                    lw=2, ls="-")
            elif self.network == "roads":
                plot_kw = dict(
                    zorder=2,
                    color="k", alpha=0.3,
                    lw=1, ls=":")

            self.data.plot(
                ax=ax, **plot_kw)

    def plot_road_types(
        self, ax,
        legend_kw = dict(bbox_to_anchor=(1.01, 1), loc='upper left', frameon=False)
        ):
        """
        Plotting routine for OsmGraphs road types. Expects ax.
        """
        if self.data is None or self.network != "roads":
            print("! OsmGraph %s: Nothing to plot." % self.network)
        else:
            for cat in self.road_categories:
                self.data.query("'%s' in highway_str" % cat).plot(
                    ax=ax,
                    label=cat,
                    color=self.road_colors[cat],
                    lw=1
                    )
    
            ax.legend(**legend_kw)


#######################################################
##### Automatic OSM Lookup from Overpass API
##### by Erik Nixdorf
#####
#######################################################
from .roverweb import geometry,osm
import geopandas as gpd
from copy import deepcopy
def retrieve_osm_overpass_data(data, lon='LongDec',lat='LatDec',crcl_radius=10,
                               no_of_clusters=None,
                               queryfeatures={'way': ['highway']},
                               output_col_name='road'):
    """
    A function to call the roverweb overpass osm module to get osm parameters for
    the desired location and time. For more details check OVERPASS API docs
    Warning: EPSG: 4326 is silently assumed

    Parameters
    ----------
    data : dataframe
        DESCRIPTION.
    lon : str, optional
        data_subset Column name for longitude coordinate. The default is 'LongDec'.
    lat : str, optional
        data_subset Column name for latitude coordinate . The default is 'LatDec'.
    crcl_radius : int, optional
        The radius of the circles created aroung each measurement point. The default is 10.
    no_of_clusters : int, optional
        The amount of cluster to be generated prior to calling Overpass API.
        The default is None, which means that the number of clusters is auto-generated
        from number of datapoints
    queryfeatures : dictionary, optional
        Features which are to retrieve from Overpass API. The default is {'way': ['highway']}.

    Returns
    -------
    data : TYPE
        Output dataframe with new column with new column entries

    """
    #if lon and lat is a float, we add this as extra columns
    if isinstance(lat,float) and isinstance(lon,float):
        data['LatDec']=deepcopy(lat)
        data['LongDec']=deepcopy(lon)
        lat='LatDec'
        lon='LongDec'
    #remove rows with no geographic coordinate
    data = data[data[lon].notna()]
    data = data[data[lon].notna()]
    #create geodataframe
    data=gpd.GeoDataFrame(data,geometry=gpd.points_from_xy(data[lon],data[lat]),crs='epsg:4326')
    #create circles
    data_crlcs = geometry.points_to_circle(data, crcl_radius=crcl_radius,
                                              number_of_points=10)
    #create number of clusters
    if no_of_clusters is None:
        print('automatic cluster generation')
        clusters=int(np.floor(np.log(len(data_crlcs))))
    else:
        clusters=int(no_of_clusters)
    #check whether at least one cluster will be generated
    if clusters<1:
        clusters=1
    #create the clusters
    data_clust = geometry.clustering(
        data_crlcs, clusters=clusters, cluster_fld_name='ClusterID')
    # group geodataframe
    data_grouped = data_clust.groupby('ClusterID')
    # create an empty result geodataframe
    output_data = gpd.GeoDataFrame()
    # inititate the loop
    for cluster_no in range(0, clusters):
        # create a subset of the original geodataframe
        data_subset = data_grouped.get_group(cluster_no)
        # create the queryboundstr
        querygeobound = osm.querybound_generation(data_subset)
        #call overpass API
        data_subset = osm.apnd_from_overpass(data_subset,querygeobound,
                                             queryfeatures=queryfeatures)

        # append subset back to entire dataset
        output_data = pandas.concat([output_data, data_subset], sort=True)

    #remove geometry
    output_data.drop(columns='geometry',inplace=True)
    #rename column from osm output
    output_data=output_data.rename(columns={output_data.iloc[:,-1].name:output_col_name})
    #sort it again, after mixing by cluster algorithm
    output_data=output_data.sort_index(ascending=True)
    return pd.DataFrame(output_data)



### OSMNX Graphs ####################################

import osmnx as ox
ox.utils.config(data_folder='data')
import os

class Graph:
    def __init__(self, network='roads', boundary=None, lats=None, lons=None, file=None, report_save='Saved'):
        if not isinstance(boundary,Polygon):
            print(' make a box')
            self.boundary = self.make_bbox(None, lats, lons)
        else:
            self.boundary=boundary
        self.network = network
        if file is None:
            file = self.network + '.graphml'
        self.file = file
        self.report_save = report_save

        if self.network == 'roads':
            self.lw = 1
            self.ls = '-'
            self.color = 'black'
            self.alpha = 0.1
            self.network_type='all_private'
            self.infrastructure='way["highway"]'
            self.custom_filter='["highway"~"residential|track|service|motorway|trunk|primary|secondary|tertiary|' \
                +'unclassified|road|motorway_link|trunk_link|primary_link|secondary_link|tertiary_link"]'

        elif self.network == 'rails':
            self.lw = 1
            self.ls = '--'
            self.color = 'black'
            self.alpha = 0.5
            self.network_type = None #'none'
            self.infrastructure = 'way["railway"]'
            self.custom_filter = None

        elif self.network == 'rivers':
            self.lw = 1
            self.ls = '-'
            self.color = '#97B6E1'
            self.alpha = 0.1
            self.network_type = "all" #'none'
            self.infrastructure = "none" #'way["waterway"]'
            self.custom_filter = '["waterway"~"river"]'

    def make_bbox(self, bbox=None, lats=None, lons=None):

        if bbox is None:
            if lats is None or lons is None:
                default_bbox = [11.030000, 11.06633, 51.640000, 51.670000]
                print('No lats/lons given, assuming bbox', default_bbox)
                return(default_bbox)
            else:
                lats = list(lats)
                lons = list(lons)
                return([min(lons), max(lons), min(lats), max(lats)])
        else:
            return(bbox)

    def download(self, network_type=None, infrastructure=None, custom_filter=None,
                 timeout=180, memory=None, max_query_area_size=2500000000):
        """


        Parameters
        ----------
        network_type : TYPE, optional
            DESCRIPTION. The default is None.
        infrastructure : TYPE, optional
            DESCRIPTION. The default is None.
        custom_filter : TYPE, optional
            DESCRIPTION. The default is None.
        timeout : TYPE, optional
            DESCRIPTION. The default is 180.
        memory : TYPE, optional
            DESCRIPTION. The default is None.
        max_query_area_size : TYPE, optional
            DESCRIPTION. The default is 2500000000.

        Returns
        -------
        None.

        """

        #ox.utils.config(all_oneway=True)

        if network_type is None: network_type = self.network_type
        if infrastructure is None: infrastructure = self.infrastructure
        if custom_filter is None: custom_filter = self.custom_filter

        print('Downloading OSM network graph...', end='')
        if isinstance(self.boundary,Polygon):
            print('Download Road Network using Polygon')
            self.G = ox.graph_from_polygon(self.boundary,
                                           simplify=True, truncate_by_edge=True, retain_all=True,
                                           network_type  =network_type,
                                           custom_filter =custom_filter)
        else:
            print('Download Road Network using a BoundaryBox')
            bbox=self.boundary
            self.G = ox.graph_from_bbox(bbox[3], bbox[2], bbox[0], bbox[1],
                        simplify=True, truncate_by_edge=True, retain_all=True,
                        network_type  ="all",
                        custom_filter ='["waterway"~"river"]')
                # infrastructure='way["waterway"]'
            print("Done!")

        
        self.data = self.graph2gdf()

        if len(self.data)<=0:
            print(' no roads found!')
        else:
            print(' OK.')
        return(self)

    def export_csv(self, prefix=''):
        file_nodes = ox.settings.data_folder + '/' + prefix + 'nodes.csv'
        file_edges = ox.settings.data_folder + '/' + prefix + 'edges.csv'
        self.graph2gdf(nodes=True, edges=False)[['y','x','osmid']].to_csv(file_nodes)
        report_size(file_nodes, self.report_save)
        self.graph2gdf(nodes=False, edges=True)[['u','v','osmid','highway','oneway','length']].to_csv(file_edges)
        report_size(file_edges, self.report_save)

    def save(self, file=None):
        if file is None:
            file = self.file
        else:
            self.file = file

        if self.G:
            ox.save_graphml(self.G, os.path.join('.','output',file))
            report_size(os.path.join('.','output',file), self.report_save)

        return(self)

    def load(self, file=None):
        if file is None:
            file = self.file
        else:
            self.file = file

        if os.path.exists(   ox.settings.data_folder+'/'+file):
            report_size(     ox.settings.data_folder+'/'+file, 'Loading')
            self.G = ox.load_graphml(ox.settings.data_folder+'/'+file)
            self.data = self.graph2gdf()

        return(self)


    def graph2gdf(self, graph=None, nodes=False, edges=True):
        if graph is None:
            graph = self.G
        gdf = ox.graph_to_gdfs(graph, nodes=nodes, edges=edges, fill_edge_geometry=True)
        return(gdf)

    def get(self, file=None, network_type=None, infrastructure=None, custom_filter=None, save=False):
        if file is None:
            file = self.file
        else:
            self.file = file

        if os.path.exists(ox.settings.data_folder+'/'+file):
            #and network_type==self.network_type and infrastructure==self.infrastructure and custom_filter==self.custom_filter:
            #print('> Loading network file %s' % file)
            self.load(file)
        else:
            self.download(network_type=network_type, infrastructure=infrastructure, custom_filter=custom_filter)
            if save:
                self.save(file)

        return(self)

    # Plot

    def plot(self, ax=None, lw=None, ls=None, color=None, alpha=None):

        if lw is None:    lw =    self.lw
        if ls is None:    ls =    self.ls
        if color is None: color = self.color
        if alpha is None: alpha = self.alpha

        if ax is None:
            ox.plot_graph(self.G, edge_linewidth=lw, edge_color=color, edge_alpha=alpha)
        else:
            self.data.plot(ax=ax, lw=lw, ls=ls, color=color, alpha=alpha)

    # Analysis

    def nearest(self, point=(0,0)):
        if 'geometry' in self.data:
            self.data['distance'] = [Point(tuple(reversed(point))).distance(line) for line in self.data['geometry']]
            return(min(list(set(self.data['distance'].values.tolist()))))
        else:
            self.data['distance'] = np.nan
            return(np.nan)

    def on_road_type(self, point=(0,0), limit=0.0001):
        distance = self.nearest(point)
        type_of_nearest = ''
        if distance < limit:
            type_of_nearest = self.data.loc[self.data['distance'] == distance, 'highway'].values[0]
        else:
            type_of_nearest = ''
        # flatten list according to https://stackoverflow.com/questions/952914/how-to-make-a-flat-list-out-of-list-of-lists
        #print([item for sublist in type_of_nearest for item in sublist])
        return(type_of_nearest)
