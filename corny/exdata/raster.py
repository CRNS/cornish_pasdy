"""
CoRNy exdata.NM
    Get Data from NMDB
"""

from corny import *


import rasterio
import rasterio._shim
import rasterio.control
import rasterio.crs
import rasterio.sample
import rasterio.vrt
import rasterio._features
import rasterio.rpc
#pandas.options.mode.chained_assignment = None  # Skip annoying warnings

def get_from_raster(data, var, file):

    if var == 'bd':
        def function(x): return(float(x)/100)
    elif var == 'clay':
        def function(x): return(float(x)/1000)
    elif var == 'org':
        def function(x): return(float(x)/10000)
    elif var == 'luse':
        def function(x): return(int(x))
    else:
        def function(x): return(float(x))

    with rasterio.open(file) as src:
        data[var] = [function(x) for x in src.sample(zip(data.lon, data.lat))]

    if var == 'luse':
        data['luse_str'] = luse_code2str(data)

    return(data)

def luse_code2str(data, luse='luse'):
    luse_cat = ['urban', 'agriculture', 'wetland', 'forest', 'water']
    luse_corineid = [1,2,4,3,5]

    data['luse_str'] = np.nan
    for i in np.arange(len(luse_corineid)):
        data.loc[data[luse] // 100 == luse_corineid[i], 'luse_str'] = luse_cat[i]

    return(data['luse_str'])

def luse_code2color(data, luse='luse'):
    luse_col = ['red', 'orange', 'lightblue', 'green', 'blue']
    luse_corineid = [1,2,4,3,5]

    data['luse_col'] = 'black'
    for i in np.arange(len(luse_corineid)):
        data.loc[data[luse] // 100 == luse_corineid[i], 'luse_col'] = luse_col[i]

    return(data['luse_col'])
