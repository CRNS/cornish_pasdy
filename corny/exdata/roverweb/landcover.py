# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 19:54:35 2021
https://gis.stackexchange.com/questions/299567/reading-data-to-geopandas-using-wfs
https://requests.readthedocs.io/en/master/user/quickstart/
https://stackoverflow.com/questions/45552955/loading-json-into-a-geodataframe
#string='https://sgx.geodatenzentrum.de/wfs_clc5_2018?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&TYPENAME=clc5_2018:clc5&BBOX=12,50,12.1,50.1,EPSG:4326&SRSNAME=v'
We download CORINE LANCOVER DATA FROM GERMANY USING OGC WFS
@author: Erik
"""

import geopandas as gpd
import requests
import xmltodict
import numpy as np
from shapely.geometry import Polygon
import pandas as pd
import geog
#%%
def generate_bounds(inpt_data,buffer=0.03):
    """


    Parameters
    ----------
    inpt_data : TYPE
        DESCRIPTION.

    Returns
    -------
    bounds : TYPE
        DESCRIPTION.
    data_type : TYPE
        DESCRIPTION.

    """
    # check source coordinate system and convert to EPSG 4326
    src_epsg = inpt_data.crs
    if src_epsg != 'epsg:4326':
        inpt_data['geometry'] = inpt_data['geometry'].to_crs(epsg=4326)
        inpt_data=inpt_data.to_crs(epsg=4326)
        print('geometry converted to EPSG:4326 for processing')

    # get the total bounds of the rover/station data
    bounds=inpt_data.total_bounds
    #check whether the distance is zero, if yes we have spatially constant data
    if int(geog.distance([bounds[0], 0],[bounds[2], 0]))==0 and int(geog.distance([bounds[0], 0],[bounds[2], 0]))==0:
        data_type='station'
    else:
        data_type='moving'

    #if distances are small we add add 2km which is 0.02 degree
    if int(geog.distance([bounds[0], 0],[bounds[2], 0]))<0.0001:
        bounds[2]=bounds[2]+buffer
        bounds[0]=bounds[0]-buffer
    if int(geog.distance([bounds[1], 0],[bounds[3], 0]))<0.0001:
        bounds[3]=bounds[3]+buffer
        bounds[1]=bounds[1]-buffer
    return bounds, data_type,inpt_data


#%% get_wfs
def get_wfs_data(bounds=tuple((12,50,12.1,50.1)),crs='EPSG:4326',parameter_name='clc5_2018:clc5'):
    """


    Parameters
    ----------
    bounds : TYPE, optional
        DESCRIPTION. The default is tuple(12,50,12.1,50.1).
    crs : TYPE, optional
        DESCRIPTION. The default is 'EPSG:4326'.
    parameter_name : TYPE, optional
        DESCRIPTION. The default is 'clc5_2018:clc5'.

    Returns
    -------
    None.

    """

    #%% make the BBOX String from the information provided
    bbox_string= ''.join(str(bounds))[1:-1]
    bbox_string+=','+crs.upper()


    query_parameters={'SERVICE':'WFS',
            'VERSION':'1.1.0',
            'REQUEST':'GetFeature',
            'TYPENAME': parameter_name,
            'BBOX': bbox_string,
            'SRSNAME':crs.upper()}
    #get data from URL
    r = requests.get('https://sgx.geodatenzentrum.de/wfs_clc5_2018', params=query_parameters)
    #%% Try to get the data very simple by directly reading the content into geopandas
    try:
        corine_gdf = gpd.read_file(r.url)
    except Exception as e:
        print(e)
        print('Error with loading geometries using hig-level routine',end='')
        # try low level
        print('...try download and conversion using low level routines')
        response = requests.request("GET", r.url)
        #get the list with the data
        features= xmltodict.parse(response.text)['wfs:FeatureCollection']['gml:featureMembers'][parameter_name]
        #for each feature we extract the landcover code

        #geometry in an ugly for loop
        features_geometry=list()
        features_property=list()
        for feature in features:
            try:
                geometry_type=list(feature['clc5_2018:geom'].keys())[0]
                vertices_str=feature['clc5_2018:geom'][geometry_type]['gml:exterior']['gml:LinearRing']['gml:posList']
                vertices=np.reshape(np.array(vertices_str.split(' ')).astype(float),(-1, 2))
                #append the geometry:
                features_geometry.append(Polygon(tuple(vertices)))
                #append the value
                features_property.append(int(feature[parameter_name[:-1]]))
            except:
                print('Feature does not contain valid geometry, continue with remaining')
                continue
        #make a dataframe
        corine_gdf=gpd.GeoDataFrame({'clc5_2018':features_property},geometry=features_geometry,crs=crs)
    print('Finished Download from WFS')
    return corine_gdf


def interpolate_from_multipolygons(row,polygon_gdf,col='clc5_2018'):
    """


    Parameters
    ----------
    row : TYPE
        DESCRIPTION.
    polygon_gdf : TYPE
        DESCRIPTION.
    col : TYPE, optional
        DESCRIPTION. The default is 'clc5_2018'.

    Returns
    -------
    row : TYPE
        DESCRIPTION.

    """
    row=row.append(pd.Series({col:polygon_gdf[polygon_gdf.contains(row.geometry.centroid)][col].values[0]}))
    return row


#%%The Main Function
def get_wfs_corine_germany(inpt_data):
    """


    Parameters
    ----------
    inpt_data : TYPE
        DESCRIPTION.

    Returns
    -------
    inpt_data : TYPE
        DESCRIPTION.

    """
    # First, if dataset is shapefile, we read it to geodataframe
    if isinstance(inpt_data, str):
        inpt_data = gpd.read_file(inpt_data)

    #preprocess the input data
    bounds, data_type,inpt_data=generate_bounds(inpt_data)

    #get geodataframe
    crs=str(inpt_data.crs) #.to_epsg())
    corine_gdf=get_wfs_data(bounds=tuple(bounds),crs=crs,parameter_name='clc5_2018:clc5')
    corine_column=[col for col in corine_gdf.columns if 'clc' in col][0]
    #do the data assignment
    if data_type=='station':
        #use head only
        station_pnt=gpd.GeoDataFrame(inpt_data['geometry']).head()
        station_pnt=station_pnt.apply(lambda row: interpolate_from_multipolygons(row,corine_gdf,col=corine_column),axis=1)
        # append on geopandas dataset
        inpt_data[corine_column]=station_pnt.iloc[0,:][corine_column]
    elif data_type=='moving':
        #compute for all records
        inpt_data=inpt_data.apply(lambda row: interpolate_from_multipolygons(row,corine_gdf,col=corine_column),axis=1)
    print('Layer',corine_column, 'added to input dataset')

    return inpt_data


#%% test
#inpt_data=gpd.read_file('testdata.shp')
#output=get_wfs_corine_germany(inpt_data)
