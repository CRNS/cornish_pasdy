from paramiko import Transport, SFTPClient
import time
import errno
import logging.config

logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': True,
    'loggers': { '': {'level': 'ERROR'} }
})
logging.basicConfig(
    format='%(levelname)s : %(message)s',
    level=logging.ERROR)


# %%
class SftpClient:
    """
    Based on the code from: https://medium.com/@thapa.parmeshwor/sftp-upload-and-download-using-python-paramiko-a594e81cbcd8
    """

    _connection = None

    def __init__(self, host, port, username, password):
        self.host = host
        self.port = port
        self.username = username
        self.password = password

        self.create_connection(self.host, self.port,
                               self.username, self.password)

    @classmethod
    def create_connection(cls, host, port, username, password):

        transport = Transport(sock=(host, port))
        transport.connect(username=username, password=password)
        cls._connection = SFTPClient.from_transport(transport)

    @staticmethod
    def uploading_info(uploaded_file_size, total_file_size):

        logging.info(
            'Upload status: %5.0f/%5.0f KB (%3.0f %%)' % (
                uploaded_file_size/1024,
                total_file_size/1024,
                (uploaded_file_size/total_file_size*100)))

    def upload(self, local_path, remote_path, progress=True):

        if progress:
            progress = self.uploading_info
        else:
            progress = None

        self._connection.put(localpath=local_path,
                             remotepath=remote_path,
                             callback=progress,
                             confirm=True)

    def folder_list(self, remote_path):
        r = self._connection.listdir(remote_path) #
        return(r)

    def file_exists(self, remote_path):

        try:
            # print('remote path : ', remote_path)
            self._connection.stat(remote_path)
        except (IOError, e):
            if e.errno == errno.ENOENT:
                return False
            raise
        else:
            return True

    def file_size(self, remote_path):

        try:
            stats = self._connection.stat(remote_path)
        except (IOError, e):
            if e.errno == errno.ENOENT:
                return None
            raise
        else:
            return stats.st_size


    def download(self, remote_path, local_path, retry=5, bytes=False):

        if self.file_exists(remote_path) or retry == 0:
            if bytes:
                self._connection.getfo(
                    remote_path,
                    local_path,
                    callback=None
                    )
            else:
                self._connection.get(
                    remote_path,
                    local_path,
                    callback=None
                    )
        elif retry > 0:
            time.sleep(5)
            retry = retry - 1
            self.download(remote_path, local_path, retry=retry, bytes=bytes)
    
    def close(self):
        self._connection.close()

