import numpy as np
import pandas
from scipy.optimize import curve_fit

class Find_Fit_Function():
    def __init__(self, data, x1=None, x2=[], x1_name='', x2_name_func=lambda x: x):
        
        self.x1_name      = x1_name
        self.x2_name_func = x2_name_func
        self.data = data
        if x1 is None:
            x1 = data[x1_name].values
        self.x1 = x1
        self.x2 = x2
        
        self.prepare_data()
    
    def prepare_data(self):
        x1 = self.x1
        x2 = self.x2
        # First, define x_data, the variables x1, x2 that describe the target values in y_data
        # Combine them to a 2D array, then flatten to a 1D array
        x1_list, x2_list = np.meshgrid(x1, x2)
        x_data = np.c_[x1_list.flatten(), x2_list.flatten()]
        
        # Now calculate the target values in y_data 
        y_data = self.y_calc_target(x_data).reshape(len(x2), len(x1)).flatten()

        # If the calculation resulted in any NaN values, remove them from x_data and y_data
        self.x_data = x_data[~np.isnan(y_data)]
        self.y_data = y_data[~np.isnan(y_data)]
    
    def fit(self, func, x2=None, p0=[1,1,1,1], bounds=([1,-10,-10,-10],[100,10,10,10]), clean_x1_out=False):
        self.fit_func = func
        if not x2 is None:
            self.x2 = x2
            self.prepare_data()
        
        print('%s() fitting with x1=%s, x2=%s' % (func.__name__, self._pretty_print_list(self.x1), self._pretty_print_list(self.x2)))
        try:
            pars, cov = curve_fit(f=self.fit_func_wrapper, xdata=self.x_data, ydata=self.y_data, p0=p0, bounds=bounds)
        except RuntimeError:
            print("  ! No fit found for default 400 iterations! Trying again with 4000 iterations...")
            try:
                pars, cov = curve_fit(f=self.fit_func_wrapper, xdata=self.x_data, ydata=self.y_data, p0=p0, bounds=bounds, maxfev=4000)
            except RuntimeError:
                print("  ! Still no fit found. Returning None.")
                return(None)
        
        self.params = pars
        self.stdevs = np.sqrt(np.diag(cov))
        
        # Determine Quality of Fit following https://stackoverflow.com/questions/19189362/getting-the-r-squared-value-using-curve-fit
        residuals = self.y_data - self.fit_func_wrapper(self.x_data, *self.params)
        ss_res = np.sum(residuals**2)
        ss_tot = np.sum((self.y_data - np.mean(self.y_data))**2)
        R_squared = 1 - (ss_res / ss_tot)

        print('  Params:  a = %+.5f,  b = %+.5f,  c = %+.5f,  d = %+.5f' % tuple(self.params))
        print('  Stdevs: da = %+.5f, db = %+.5f, dc = %+.5f, dd = %+.5f' % tuple(self.stdevs))
        print('  Performance: SS_res = %.3f, SS_tot = %.3f, R²=%.3f' % (ss_res, ss_tot, R_squared))
        
        D = pandas.DataFrame()
        D['x1'] = self.x_data[:,0]
        D['x2'] = self.x_data[:,1]
        D['y'] = self.y_data
        D['y_pred'] = self.fit_func_wrapper(self.x_data, *self.params)
        
        if clean_x1_out:
            D = D.sort_values('x1').drop_duplicates('x1')
        return( dict(params=self.params, stdevs=self.stdevs, x1=D['x1'], x2=D['x2'], y=D['y'], y_pred=D['y_pred'], R2=R_squared))

        
    def y_calc_target(self, xy_pairs):
        # In this case, read out the value at row=bd and col=ratio_theta
        return( np.array([ self.data.loc[self.data[self.x1_name]==x1, self.x2_name_func(x2)] for (x1, x2) in xy_pairs ]) )
    
    
    def fit_func_wrapper(self, variables, a, b, c, d):
        # For each x, call fit_func(x)
        r = [self.fit_func(x1, x2, a, b, c, d) for (x1,x2) in variables]
        return( np.array( r ))
    
    def _pretty_print_list(self, L, max_items=5):
        if len(L) > max_items:
            r = '[' + ', '.join('%.3f' % x for x in L[0:max_items]) + ', ...]'
        else:
            r = '[' + ', '.join('%.3f' % x for x in L) + ']'
        return(r)

# Shortcut
FFF = Find_Fit_Function
