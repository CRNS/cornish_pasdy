"""
CoRNy Reports
    Functions for exporting data and plots to a PDF
"""

# Requires FPDF
# https://py-pdf.github.io/fpdf2/
# Todo: Update to PyPdf 3.*
# https://pypdf.readthedocs.io/en/latest/search.html?q=&check_keywords=yes&area=default
from fpdf import FPDF
from fpdf.enums import XPos, YPos
ln0 = dict(new_x=XPos.RIGHT, new_y=YPos.TOP)
ln1 = dict(new_x=XPos.LMARGIN, new_y=YPos.NEXT)

import os
import numpy as np
from datetime import datetime

# Corny depenencies
from corny.basics import file_save_msg
from corny.version import get_version
from corny.io import app_file_path

###############
class Journalist():
    """
    Scope:
        Can be used to log information during the processing.
        Supports different story categories.
        The list of reports can be later posted as a composite string.
        Useful for writing reports with class Report().
    Feature:
        Supports Nonetype values to be formatted gracefully.
    Usage:
        J = Journalist()
        ...
        J.report("observations",
            "Temperature today was {:.2f}.",
            None)
        ...
        J.report("observations",
            "Data was corrected following {:}, only {:d} points remained.",
            ("Brown et al. (1979)", 42))
        ...
        J.post("observations")

    Returns:
        Temperature today was nan. Data was corrected
        following Brown et al. (1979), only 42 points remained.
    """
    def __init__(self):
        self.story = dict() 
        pass

    def assert_category(self, category):
        """
        Makes sure that the category exists in dict before appending.
        Intialized as empty list per category.
        """
        if not category in self.story:
            self.story[category] = []

    def report(self, category="default", text="", *values):
        """
        Appends a text to the category's list.
        The text is checked for Nonetype values before.
        """
        self.assert_category(category)

        if values:
            # Replace all None by np.nan to avoid NoneType Error on formatting
            values = [np.nan if v is None else v for v in values]
            text = text.format(*values)

        self.story[category].append(text)

    def post(self, *categories) -> str:
        """
        Joins the category's list on a single space.
        """
        # if isinstance(category, str):
        #     category = [ category ]
        text = []
        for category in categories:
            self.assert_category(category)
            text.append( " ".join(self.story[category]) )
        
        return(" ".join(text)) 


###############
class Report():
    """
    Usage:
        with Report("example.pdf", "My Title") as R:
            R.add_page()
            R.add_title("Chapter 1)
            R.add_paragraph("Long text")
            R.add_image(buffer)
            R.add_table(data)
            ...
    """
    def __init__(self, filename, title=""):
        self.filename = filename
        self.title = title

    def __enter__(self):
        self.PDF = PDFReport()
        self.PDF.header_text = self.title
        return(self.PDF)

    def __exit__(self, type, value, traceback):
        self.PDF.output(self.filename)
        

######################
class PDFReport(FPDF):
    """
    Usage:
        pdf = PDF()
        pdf.header_text = "My title"
        pdf.add_page()
        ...
        pdf.output("my_title.pdf")
    
    Credits:
        Thanks to https://py-pdf.github.io/fpdf2/Maths.html
    """

    # These variables can be changed individually if necessary
    cell_height = 8
    header_text = ""
    font      = "Roboto"
    font_mono = "RobotoM" #"Courier" # Helvetica 
    font_size = 10

    def __init__(self):
        super().__init__()
        font_folder = app_file_path("app/_ui/fonts/")
        # font_folder = os.path.dirname(os.path.abspath(__file__)) + "/../app/_ui/fonts/"
        
        self.add_font("Roboto",  "",  font_folder+"Roboto-Regular.ttf",     uni=True)
        self.add_font("Roboto",  "B", font_folder+"Roboto-Bold.ttf",        uni=True)
        self.add_font("RobotoM", "",  font_folder+"RobotoMono-Regular.ttf", uni=True)
        self.add_font("RobotoM", "B", font_folder+"RobotoMono-Bold.ttf",    uni=True)
        


    def header(self):
        """
        Overwrites FPDF's header function.
        | %title | Corny %version | %datetime | %page |
        """

        self.set_font(self.font, 'B', self.font_size)
        self.cell(self.epw-35-45-15, 8,
                  " %s" % self.header_text,
                  border=True, align='L', **ln0)
        
        self.set_font(self.font_mono, '', self.font_size)
        self.cell( 35, 8,
                  "Corny %s" % get_version(),
                  border=True, align='C', **ln0)
        self.cell( 45, 8,
                  datetime.now().strftime("%Y-%m-%d %H:%M"),
                  border=True, align='C', **ln0)
        self.cell( 15, 8,
                  "%2s " % str(self.page_no()),
                  border=True, align='R', **ln1)
        self.ln(self.cell_height)
        
    # def footer(self):
    #     self.set_y(-15)
    #     self.set_font('Courier', '', 12)
    #     self.cell(0, 8, f'Page {self.page_no()}', True, align='C', **ln0)

    def add_title(self, title=None, style="B"):
        """
        Add a chapter title
        """
        if title is None:
            title = self.header_text
        self.set_font(self.font, style=style, size=24)
        self.cell(w=0, h=20, txt=title, **ln1)
        self.set_font(style='', size=self.font_size)
        # self.ln(self.cell_height)

    def add_paragraph(self, text=None):
        """
        Add a multiline paragraph.
        """
        if text is None:
            text = ""
        self.multi_cell(w=0, h=5, txt=text)
        self.ln(self.cell_height)

    add_par = add_paragraph

    def add_image(self, source=None,
                  x=None, y=None, w=None, h=0, link = ''):
        """
        Can be a file path (png, jpg) or an image buffer.
        For image buffers use before:
            img_buf = BytesIO()
            plt.savefig(img_buf, format="svg")
        Or use Corny:
            from corny.figures import Figure
            with Figure(size=(5,5), save="buff") as F:
                ax = F.axes
                ax.scatter(data=df)
            img_buf = F.buff
        """
        if w is None:
            w = self.epw
        self.image(source, x=x, y=y, w=w, h=h, link=link)
        self.ln(self.cell_height)

    def add_table(self, data=None, align="RIGHT"):
        """
        Add a table, just provide a pandas DataFrame
        """
        
        self.set_font(self.font_mono, size=7)
        
        data["Date"] = data.index.strftime("%Y-%m-%d")
        cols = list(data.columns)
        cols = [cols[-1]] + cols[:-1]
        data = data[cols]

        data = data.astype(str)
        columns = [list(data)]  # Get list of dataframe columns
        rows = data.values.tolist()  # Get list of dataframe rows
        data = columns + rows  # Combine columns and rows in one list

        with self.table(borders_layout="SINGLE_TOP_LINE",
                    cell_fill_color=245,
                    cell_fill_mode="ROWS",
                    line_height=self.font_size*0.5,
                    text_align=align,
                    width=self.epw) as table:
            for data_row in data:
                row = table.row()
                for datum in data_row:
                    row.cell(datum)
                
        self.set_font(self.font, style='', size=self.font_size)
        self.ln(self.cell_height)

