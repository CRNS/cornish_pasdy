# Corny module: version.py
# Version management
VERSION = "0.8.3"

from .io import app_file_path

def get_version():
    """
    Returns the current Version of Corny.
    Usage:
        v = corny.version.get_version()
        # or:
        from corny.version import __version__
        v = __version__
    """
   
    # with open(app_file_path("VERSION")) as f:
    #     return(f.read().strip())

    return(VERSION)

__version__ = get_version()
