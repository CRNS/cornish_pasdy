# %%
"""
This example shows how to retrieve data from
Neutron Monitors (NMs) from nmdb.eu
"""

import corny.exdata.nmdb as nmdb

# %%
"""
Set up a NM object for station Jungfraujoch,
save all data into input/NM/,
and use time resolution of 1 hour.
"""

M = nmdb.NM(
    station    = 'JUNG',
    folder     = 'input/NM/',
    resolution = '1min'
    )

# %%
"""
Start download for given dates or datetimes.
Checks local data and downloads missing parts.
Downloads full month or year because no additional costs.
"""

M.get(
    start = '2015-05-10',
    end   = '2015-05-13'
    )

M.data

# %%
# Let's look at the data
M.data.plot()

# %%
"""
One could do additional cleaning
"""

# Set time zone, reset at first.
M.data.index = M.data.index.tz_localize(None)
M.data.index = M.data.index.tz_localize('UTC')

# Remove possible duplicates
M.data = M.data[~M.data.index.duplicated(keep='first')]

# Remove outliers with more than 20% deviation from mean
import numpy as np
M.data[M.data < M.data.mean()/1.2] = np.nan
M.data[M.data > M.data.mean()*1.2] = np.nan

# %%
"""
Merge with existing dataset.
We'll first create an examplary time series.
"""
import pandas

# Create a DataFrame with the date range as the index
df = pandas.DataFrame(
        pandas.date_range(
            start= '2015-05-10',
            end  = '2015-05-13',
            freq = 'H',
            tz   = 'UTC'
            ),
        columns=['date'])
df['counts'] = np.random.randint(0, 100, size=(len(df)))
df.set_index('date', inplace=True)
df

# %%
"""
Now do the merging on nearest times.
"""
from datetime import timedelta

df["NM"] = M.data.reindex(
    df.index,
    method = 'nearest',
    limit  = 1,
    # Provide tolerance < half resolution.
    tolerance = timedelta(
        minutes = M.resolution_min/2-0.1
        )
    # Interpolate over missing data
    ).interpolate()
df

# %%
