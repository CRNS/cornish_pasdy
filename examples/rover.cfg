# CRNS configuration
[info]

## Title page
# Name of the station or measurement campaign and additional information.
title = CRNS Test Campaign
subtitle = operated by Helmholtz Centre for Environmental Research -- UFZ, Leipzig
contact = Martin Schrön
# Displayed cover image, size should be ~800x300 pixel.
# Units: png image
cover = input/rover.png

## Meta data
# Default coordinates, if no other coordinates are provided in the data stream. Format: lat, lon.
# Units: decimal EPSG:4326
coords = 51.877, 12.237

[update]

# CoRNy can look for new data automatically right at the start.
# New data will be copied to the local storage defined in Input > path.

## Source
# The data source to be checked can be one or a combination of the following (seperate by comma): FTP (download files from an FTP server), SD (copy files from a remote folder or SD drive). Leave blank to skip updates.
# Units: SD, FTP
data_source = SD

## FTP server credentials
# Host address, username, and password (Warning: will be stored in plain text)
FTP_server = 
FTP_user = 
FTP_pswd = 
# First letters of the target path. Example: path/to/files/2021
FTP_prefix = 
# Last letters of the target path. Example: .RV1
FTP_suffix = 

## Remote folder
# Path to the folder or drive. Example: D:/raw_data/
SD_path = input/rover-SDcard
# First letters of the target file. Example: 2021
SD_prefix = 
# Last letters of the target path. Example: .RV1
SD_suffix = RV1

[input]

## Local storage
# Folder or zip archive containing the input files:
path = input/rover.zip
# First letters of the target file. Example: CRSData_2021
prefix = 190821
# Last letters of the target path. Example: .RV1
suffix = .RV1

## Cache data
# Caches the read and merged input DataFrame to a compressed file. This will greatly accelerate repeated input reading for large datasets. Use file.bz2 for compression with faster read/write. Use file.xz for compression with lower file size 
# Units: file name and extension with bz2 or xz
store_pickle = rover_example.xz
# If the pickle file exists, skip input data reading and read directly from pickle? (yes/no):
restore_from_pickle = no


## Column names
# Explicitely define columns as a comma seperated list or leave blank for automatic detection (based on one or multiple headlines starting with //). Typical columns are:
# RecordNum, Date Time(UTC), PTB110_mb, P4_mb, P1_mb, Vbat, T1_C, RH1, N1Cts, N2Cts, N1ETsec, N2ETsec, N1T_C, N1RH, N2T_C, N2RH, T_CS215, RH_CS215, GpsUTC, LatDec, LongDec, Alt, Qual, NumSats, HDOP, Speed_kmh, COG, SpeedQuality, strDate
input_columns = 

# Index column numbers. If two numbers are given, separated by comma, will join the columns by space. Example: 1, 2 (will use the second column (expecting a date string) and third column (expecting a time string)).
# Units: column index (start at 0)
index_column_number = 
# Use Unix-Timestamp format (yes/no)
timestamp = no
# Separation character of the columns, e.g. \t for tab.
sep = 
# Decimal letter, by default . but sometimes it can be ,
decimal = .
# Skip first lines
# Units: number of lines
skip_lines = 0

## Separate GPS file
# Is the GPS data in another file as the sensor data? (yes/no)
separate_gps = no
# File pattern
gps_prefix = GPS
gps_suffix = 
# Explicitely define columns
gps_input_columns = SCS,Time,NSats, lat, lon, alt, Date, Time1, Datetime
# Index column numbers. If two numbers are given, separated by comma, will join the columns by space. Example: 1, 2 (will use the second column (expecting a date string) and third column (expecting a time string)).
# Units: column index (start at 0)
gps_index_column_number = 6, 7
# Use Unix-Timestamp format (yes/no)
gps_timestamp = no
# Skip first lines
# Units: number of lines
gps_skip_lines = 0


## Neutron tube columns
# Neutron counts for epithermal (moderated) and thermal (bare) detector tubes. Multiple comma-seperated column names will be summed up to produce total count rate.
neutron_columns = N1Cts, N2Cts
# List columns that you want to ignore for this analysis. The N0 value will we rescaled accordingly.
ignore_neutron_columns = 
thermal_neutron_columns = 

# Record period (or temporal resolution) in seconds. A single column is sufficient.
resolution_column = N1ETsec
# Default resolution.
# Units: sec 
resolution_default = 60

## Atmosheric data columns
# Data from external sensors for air pressure (mbar), relative air humidity (%) and air temperature (°C). A single column for each variable is sufficient. Typical column names are: PTB110_mb, p4, p3, p1, RH_CS215, or T_CS215.
pressure_column = PTB110_mb
humidity_column = RH_CS215
temperature_column = T_CS215

## Roving
# For mobile CRNS rover data, specify column names for the coordinates. Note: Units of lat and lon are assumed in decimal EPSG:4326, units of altitude in meter.
latitude_column = LatDec
longitude_column = LongDec
altitude_column = Alt

## Muons
# Column for muon counts
muon_column = 

[clean]

# Cut the data, find outliers, fill gaps, and smooth out noise.

## Cutting
# Temporal measurement period. Format: 2021-05-23 00:42:00. Leave blank for automatic detection based on the data period.
# Units: UTC
start = 
# Units: UTC
end = 

# Spatial bounding box. Format: lon1, lon2, lat1, lat2. Leave blank for automatic detection based on the data extent.
# Units: decimal EPSG:4326
bbox = 

## Exclude areas
# Exclude certain areas (e.g. tunnels or buildings). Make sure to add spaces before each line (table):
# Table: lat, lon, radius (m)
exclude_table = 
  
## UTM conversion
# Convert lat/lon data to UTM coordinates with EPSG projection. Default is: 31468. Other examples: 25832, ...
utm_epsg = 31468
  
## Scoring
# Scoring method (select one):
# - none # none
# - Altdorff et al. (2023) # temporal
# - Altdorff et al. (2023b) # spatial
scoring_method = none
# Scoring filters, can be a combination of:
# minus_mean, minus_median, follow_SD, before_SD, wetter_before_and_after, minus_before, minus_follow, minus_mean
scoring_filters = minus_mean, minus_median, follow_SD, before_SD, wetter_before_and_after, minus_before, minus_follow, minus_mean
# Variable to apply scoring to (select one):
# - neutrons # Apply scoring on corrected neutrons
# - moisture # Apply scoring on soil moisture
scoring_var = moisture
# Scoring window considers data before and after the data point
scoring_scope = 12
# Steps larger than the std_factor times the standard deviation will be dropped
scoring_std_factor = 1.5
# Data with less than maximum score points will be kept
scoring_max_score = 2
# Spatial scoring radius:
scoring_radius = 100
# Spatial scoring averageing method (select one):
# - equal       # equal weight, i.e. simple average over all points
# - W_r_approx  # higher weighting for near points according to the neutron transport function W*_r (Schrön et al. 2017)
scoring_radius_method = W_r_approx

## Temporal aggregation
# Aggregate the neutron counts to fixed time steps. E.g., 1min, 2hour, 3day, 4week, 5month, 6year, leave blank for no aggregation.
# Units: interval
aggregate = 
# The averaging function for aggregation (select one):
# - mean    # Arithmetic mean, in units of cph
# - median  # Median, in units of cph
# - sum     # Sum of all counts per aggregation interval
aggregate_func = sum
# Sub-resolution aggregation only for visual plotting purposes
aggregate_minor_vis = 10sec

## Smoothing
# Temporal moving average (=rolling mean) over a time window (time steps). Example: a time series aggregated to daily resolution and smooth=3 will be smoothed over 3 days. 
# Units: number of data points
smooth = 3
# Spatial average over a number of nearby data points within a radius.
# Units: m
smooth_radius = 50
# Spatial weighting method of the average (select one):
# - equal       # equal weight, i.e. simple average over all points
# - W_r_approx  # higher weighting for near points according to the neutron transport function W*_r (Schrön et al. 2017)
smooth_radius_method = W_r_approx
# Spatial average over a number of next neighbours (EXPERIMENTAL):
number_locations = 

## Regular Gridding
# Resample all data points to a regular grid based on a UTM coorindate system given by utm_epsg. 
# Bounding box of the grid. Leave blank for no gridding.
grid_bbox = 
# Grid resolution
grid_resolution = 50

## Custom Gridding
# Resample all data points to new locations defined in the following master grid.  Leave blank for no spatial resampling
# Units: csv file
grid_master_file = 
# Spatial average over nearby data points within a radius (m). 
# Units: m
grid_smooth_radius = 120
# Spatial weighting method of the average (select one):
# - equal       # equal weight, i.e. simple average over all points
# - W_r_approx  # higher weighting for near points according to the neutron transport function W*_r (Schrön et al. 2017)
grid_smooth_method = equal


## Invalid data
# If data has been identified as invalid, either (select one):
# - do nothing      # will probably cause subsequent failure of procedures
# - drop lines      # drops all lines that contain invalid data,
# - replace by NaN  # replaces by a not-a-number string
invalid_data = replace by NaN

# Interpolate over invalid neutron data (yes/no):
interpolate_neutrons = no
# Maximum gap size fo interpolation
# Units: number of data points
interpolate_neutrons_gapsize = 6
# Interpolate over invalid coordinates (yes/no):
interpolate_coords = no

# units
neutrons_unit = 
pressure_unit = 

# Replace missing atmospheric data of air pressure (mbar), abs. air humidity (%), and temperature (°C) by a constant.
# Type "DWD" to consult an external source to look up for a nearby German Weather Service station. Cite as: DWD Climate Data Center (CDC): Aktuelle 10-minütige Stationsmessungen des Luftdrucks, der Lufttemperatur (in 5cm und 2m Höhe), der Luftfeuchte und des Taupunkts in Deutschland, Version recent, 2019.
# Units: mbar or DWD
missing_pressure = DWD
# Units: % or DWD
missing_humidity = DWD
# Units: °C or DWD
missing_temperature = DWD
# Replace missing altitude data with information from a local DEM raster file.
# Units: tif file
altitude_raster = 
# Replace missing information about time resolution.
# Units: sec
resolution_default = 60

## Outlier filter
# Remove unphysical and outlying values based on (min, max) ranges.

# Allowed range (a, b) of uncorrected (raw) neutron counts for each tube/column.
# Units: raw counts
neutron_range_per_tube = 1, 1000
# Allowed range (a, b) of corrected neutron counts in cph.
# Units: cph
neutron_range = 3000, 20000
# Treat every jump that is bigger than n_sigma * sqrt(N) as an outlier.
# Units: standard deviations
neutron_n_sigma = 3
# Allowed range (a, b) of the original measurement interval (record period).
# Units: sec
timeres_range = 8, 14

# Allowed ranges (a,b) for atmospheric data of air pressure (mbar), rel. air humidity (%), and temperature (°C):
# Units: mbar
pressure_range = 1, 1100
# Units: %
humidity_range = 0, 100
# Units: °C
temperature_range = -60, 60

# Allowed range (a,b) of coverted volumetric soil moisture values in units of (m³/m³)
# Units: m³/m³
sm_range = 0, 0.8

## Muon count range
# Units: cph
muon_range = 


## Split tracks
# Identify single tracks between locations A and B (yes/no)
split_tracks = no
# UTM coordinates (x, y) of the two locations.
# Units: m
split_location_A = 4427390, 5742064
# Units: m
split_location_B = 4420333, 5736203
# The radius around the locations (in meters) helps to define a safe space.
# Units: m
split_location_radius = 50
# Minimum duration for a full track.
# Units: minutes
split_min_duration = 10
# Continue Corny with only a subset of tracks. Either a single track (id), or every second track (type A->B or B->A).
# Leave blank to continue Corny with the whole dataset. 
# Units: track id or A->B or B<-A or all
split_continue_track = 


[correction]

# New column name for corrected neutrons:
new_neutron_column = N

## Air humidity
# New column name for absolute air humidity:
new_humidity_column = ah
# Method to calculate the humidity correction factor C_h (select one):
# - None                   # no humidity correction
# - Rosolem et al. (2013)  # linear correction, = 1 + alpha*(h-h_ref)
# - Koehli et al. (2021)   # polynom as a function of SM, = 1 (please select also N2sm_method = Schmidt)
humidity_method = Rosolem et al. (2013)
# Correction parameter for Rosolem et al. (2013) to scale the influence of water vapour.
# Units: m³/g
alpha = 0.0054
# Reference absolute humidity for relative correction.
# Units: g/m³
humidity_ref = 0

## Air pressure
# Method to calculate the humidity correction factor C_P (select one):
# - Zreda et al. (2012)  # exponential correction, = exp(beta*(P-P_ref))
# - Dunai et al. (2000)  # Same as Zreda, but calculate beta using magnetic field inclination
pressure_method = Zreda et al. (2012)
# Correction parameter for Zreda et al. (2012) that represents the neutron attenuation coefficient in the atmosphere. Can be estimated using <crnslab.org/util/intensity.php>. 
# Units: 1/mbar
beta = 136
# Correction parameter for Dunai et al. (2000) for the magnetic field inclination:
Dunai_inclination = 
# Reference pressure for relative correction.
# Units: mbar
pressure_ref = 1013.25

## Incoming radiation
# New column name for incoming neutron monitor data
new_incoming_column = NM
# Method to calculate the humidity correction factor C_inc, based on Schrön et al. (2015): = 1/(1 - gamma*(1-I/Iref)) (select one)
# - Zreda et al. (2012)  # simple ratio I/I_ref using gamma = 1
# - Schroen et al. (2015)  # Amplitude scaling by gamma
# - Rotunno and Zreda (2014)  # linear to local cutoff rigidity, gamma ~ Rc
# - Hawdon et al. (2014)  # Function of local and reference cutoff rigidity, gamma ~ (Rc - Rc_ref)
# - McJannet and Desilets et al. (2023)  # Rescales JUNG based on lat lon, Rc
incoming_method = Zreda et al. (2012)
# Correction parameter to scale the amplitude of the incoming radiation used for Schroen et al. (2015):
gamma = 1
# Reference incoming counts (per second) of the chosen neutron monitor.
# Units: cps
incoming_ref = 150
# Cutoff rigidity R_c of the local sensor used for Rotunno and Zreda (2014) and Hawdon et al. (2014).
# Units: GV
Rc = 
# Cutoff rigidity of the neutron monitor used for Hawdon et al. (2014).
# Units: GV
Rc_ref = 

## Reference Neutron Monitor
# Automatically download NM data from <nmdb.eu> (yes/no):
NM_auto_download = yes
# Folder into which NM data should be written.
# Units: folder
NM_path = input/NM/
# Choose a NM station, e.g. JUNG for Jungfraujoch (Switzerland). See also: <NMDB.eu>. 
# Units: NMDB short name
NM_station = JUNG
# Resolution of the NM data, should be lower or equal to the CRNS data (select one):
# - 1min  #  1 minute  temporal resolution
# - 10min # 10 minutes temporal resolution
# - 1hour #  1 hour    temporal resolution
# - 1day  #  1 day     temporal resolution
NM_resolution = 1min

## Muon counts
# Muon correction parameters based on Stevanato et al. (2022)
muon_beta = 625
muon_alpha = 0.0021
muon_T_ref = 0

[conversion]

# New column name for the soil moisture product
new_moisture_column = moisture

## Soil properties
# Get soil data from (select one):
# - constant  # set a number below
# - raster  # provide a path to the geotiff file below
# - soilgrids  # download from <soilgrids.org>
soil_data_source = constant

# Soil organic carbon content in g/g.
# Units: g/g
soil_org_carbon = 0
# Units: tif file
soil_org_carbon_raster = 
# Method to convert to organic water equivalent (select one):
# - Franz et al. (2015)  # oew = 0.5556* soc
owe_method = Franz et al. (2015)

# Lattice water content in g/g.
# Units: g/g
lattice_water = 0
# Units: tif file
clay_content_raster = 
# Method to convert clay content to lattice water (select one):
# - Greacen et al. (1981)  # lw = clay% * 0.1783
lw_method = Greacen et al. (1981)

# Soil bulk density and its absolute uncertainty.
# Units: g/cm³
bulk_density = 1
# Units: kg/m³
bulk_density_err = 0
# Units: tif file
bulk_density_raster = 

## Landuse
# Get landuse information from (select one):
# - none    # none
# - constant  # set a number below
# - raster  # provide a path to the geotiff file below
# - corine  # download from CORINE data base (EXPERIMENTAL)
land_use_data_source = none
# Units: tif file
land_use_raster = 
# Correction factors and their uncertainties for urban areas or forests, type nan to exclude those regions:
land_use_urban_corr = nan
land_use_forest_corr = 1.1
land_use_forest_corr_err = 0.0

## Vegetation correction
# Method to correct for vegetation C_veg (select one):
# - none  # none
# - Baatz et al. (2015)  # scale neutrons by a percentage of biomass
# - Jakobi et al. (2018)  # make use of the epithermal/thermal ratio
veg_method = none

# Local biomass at the site.
# Units: kg/m²
biomass = 0
# Parameter for biomass correction:
# Units: m²/kg
nu = 0.009248

## Snow correction
# Method to correct for vegetation C_veg (select one):
# - none # none
# - Schattan et al. (2017) # scale neutrons by a percentage of snow water equivalent
snow_method = none
# Data source for SWE data used in Schattan et al. (2017).
# Units: csv file
snow_data_source = 
# Local snow water equivalent in mm.
# Units: mm
SWE = 0

## Rain
# Additional rain data.
# Units: csv file
rainfall_data_source = 
# Units: mm
rainfall = 0

## Road effect
# Method to correct for road material or other local effects C_road (select one):
# - none # none
# - Schroen et al. (2018)  # based on road material, width, and distance
road_method = Schroen et al. (2018)
# Lookup table of road types and its attributes.
# Units: csv file
road_type_table = input/road_type_correction.txt
# Default values for local moisture (m³/m³) and road width (m) when no road data is available.
# Units: m³/m³
road_moisture = 0.07
# Units: m
road_width = 0
# Relative uncertainty (-) of the road correction factor C_road:
road_correction_err = 0.0
# Get road type from an external source (select one):
# - none    # none
# - osm-nx  # Use package OSMNX to parse the network graph from OpenStreetMap
# - osm-overpass  # Access OpenStreetMap online using Overpass API (EXPERIMENTAL)
road_network_source = osm-nx
# File to store the road network data such that download is only required once. Please only specify a filename.graphml, osmnx will create a folder "data" to place it. Note: The data of the nearest road per measurement will be saved to the output path with suffix .roads, this avoids reprocessing of nearest roads every time a dataset is reevaluated.
# Units: graphml file
road_network_file = road_network.graphml

## Areal correction
# PNG image of the 500x500 m² area around the sensor. RGB colors represent soil moisture as rgb=sm%*2. Where 20=0.1 m³/m³, 254=water, 204=concrete, etc (see URANOS). The area of interest showig soil moisture dynamics is RGB=100
areal_correction_map = 

## Neutrons to Soil moisture
# Method to convert corrected neutrons to soil moisture (select one):
# - Desilets et al. (2010)  # = a0/(N/N0-a1)-a2 - lw - soc
# - Schmidt et al. (2021)  # = inverter for N(theta, h) based on Koehli et al. (2021)
N2sm_method = Desilets et al. (2010)
# Only Desilets et al. 2010: Numerical parameters of the Desilets equation.
# Units: m³/m³
a0 = 0.0808
a1 = 0.372
# Units: m³/m³
a2 = 0.115
# Free calibration parameter in cph, leave blank if it should be calibrated.
# Units: cph
N0 = 12447
# Only Schmidt et al. 2021: Parameterset to use for conversion (select one):
# atmprof = atmospheric profile, drf = detector response function, ewin = energy window response, gd = gadolinum shield. Refer to Markus Köhli for details.
# - Mar12_atmprof         # ...
# - Mar21_mcnp_drf        # ...
# - Mar21_mcnp_ewin       # ...
# - Mar21_uranos_drf      # ...
# - Mar21_uranos_ewin     # ...
# - Mar22_mcnp_drf_Jan    # ...
# - Mar22_mcnp_ewin_gd    # ...
# - Mar22_uranos_drf_gd   # ...
# - Mar22_uranos_ewin_chi # ...
# - Mar22_uranos_drf_h200 # ...
# - Aug08_mcnp_drf        # ...
# - Aug08_mcnp_ewin       # ...
# - Aug12_uranos_drf      # ...
# - Aug12_uranos_ewin     # ...
Schmidt_parameterset = Aug08_mcnp_drf

## Calibration
# If N0 is unknown, leave N0 blank and activate calibration (yes/no):
calibrate_N0 = no
# Set a start and end datetime of the calibration period. Format: 2021-05-23 00:42:00. 
# Units: UTC
campaign_start = 2020-06-24 08:20:00
# Units: UTC
campaign_end = 2020-06-24 08:30:00
# Soil moisture (in m³/m³) measured during the calibration campaign.
# Units: m³/m³
measured_sm = 0.20

## Points of interest
# Neutrons and soil moisture will be evaluated at that point, e.g. to quickly compare the CRNS result at certain locations.
# Make sure to add spaces before each line (table):
# Table: name, lat, lon, radius (m)
poi_table = 
  river_plain,   51.877578, 12.244764,  50
  road_junction, 51.877191, 12.237821, 100  

# Spatial weighting method of the average (select one):
# - equal       # equal weight, i.e. simple average over all points
# - inverse     # inverse weighting by distance, ~1/r
# - W_r_approx  # higher weighting for nearer points according to the neutron transport function W*_r (Schrön et al. 2017)
poi_distance_weight = equal
# Add POIs to the spatial maps in the output? (yes/no)
poi_plot = yes

## Estimate Footprint
# Calculate the footprint radius (yes/no): 
estimate_footprint = yes

[output]

## Target directory
# Path to output files, leave blank for the current folder.
# Units: folder
out_path = output/
# File basename for all output files. Corny will attach a time stamp and file extensions.
out_basename = rover

## CSV
# Export a comma-seperated data file (yes/no):
make_CSV = yes
# Select columns to save, leave blank to save all columns.
# Units: list of column names
# Examples: lat, lon, alt, pressure, temperature, relative_humidity, neutrons_raw, neutrons_proc, neutrons_proc_err, moisture_vol, moisture_vol_err_low, moisture_vol_err_upp, footprint_radius, footprint_depth
CSV_columns = 

# Date format (Y=year, m=month, d=day, H=hour, M=minute, S=second).
# Units: Y-m-d H:M:S
CSV_datetime_format = 
# Float format for all numbers. Example: x.yf (x is the full length and y the decimal precision)
CSV_float_format = 11.5f
# Decimal character, e.g., . or ,
CSV_decimal = .
# Column separating character. Example: ,
CSV_column_sep = ","
# Representation of Not-a-Number (=invalid) values in the output file, Examples: NaN, NA, -9999,
CSV_NaN = 

## Summary
# Export summary data and basic statistics of the dataset and, if roving and split tracks is used, shows statistics for each track (yes/no).
make_summary = no


## PDF
# Export plots to pdf file (yes/no):
make_PDF = yes
# Plot more columns (default is: N, pressure, rel humidity, temperature, abs humidity, moisture)
# Special plots are: tubes, footprint, diurnal, map_xy, map_tiles, map_interpolation, roads
PDF_plots = footprint_length, footprint, tubes, map_grid, map_tiles, map_interpolation, roads
# Displayed data range for vol. soil moisture in the plots.
# Units: m³/m³
PDF_sm_range = 0, 0.5
# Resolution of spatial grids, e.g. for map interpolation.
grid_resolution = 50
grid_resol_fine = 2.1
grid_markersize = 100
# Time format for x-axis in plots  (Y=year, m=month, d=day, H=hour, M=minute, S=second).
# Units: Y-m-d H:M:S
PDF_time_format = H:M
# Line style fallback if no explicit styles were defined (select one):
# - points # point style (default)
# - lines  # Line style
# - steps  # Steps style
PDF_line_style = points
# Satellite picture zoom level. Warning: using high zoom levels for large extent maps could crash the Google map service. Better use low resolution, e.g., zoom = 10, for faster performance.
# Units: Google zoom level 1--18
satellite_zoom = 16
# Map tiles to use for mapping (select one):
# - osm              # OpenStreetMap (default)
# - google           # Google Maps
# - satellite-google # Google Maps satellite
# - satellite-ms     # Microsoft Bing satellite 
# - stamen           # Stamen terrain 
# - toner            # Stamen toner
# - watercolor       # Stamen water color
tiles = satellite-ms

# Interpolation options if "map_interpolation" is in "PDF_plots".
# Variogram model used for Ordinary Kriging interpolation (select one):
# - spherical   # Spherical model (default)
# - linear      # Linear model
# - power       # Power model
# - gaussian    # Gaussian model
# - exponential # Exponential model
variogram = spherical
# Spatial resolution of the interpolated grid.
# Units: lat/lon degree, e.g. 0.001 (equals ~70 meters)
interpolation_ll_resolution = 0.001
# Resolution of the colorbar.
# Units: m³/m³
interpolation_sm_resolution = 0.02
# Export raster map as ASCII Grid using the ESRI standard (yes/no):
interpolation_export = yes
# Add a river network to the plot using OSM (yes/no):
interpolation_plot_rivers = yes
# Add a road network to the plot using OSM (yes/no):
interpolation_plot_roads = yes

## PNG
# EXPERIMENTAL: Export a certain plot to a png file and upload it to a webserver directly (yes/no):
make_PNG = no
# Make plots (only available for now: map_tiles_flyer)
PNG_plots = map_tiles_flyer
# Upload the image file to the path given below using the FTP server specified under [update]
PNG_upload_ftp_path = 


## KML
# Export georeferenced KML file for neutrons (yes/no):
make_KML_neutrons = no
# Scale colors according to the range min/max.
# Units: cph
KML_neutrons_range = 
# Export georeferenced KML file for soil moisture (yes/no):
make_KML_sm = yes
# Scale colors according to the range min/max.
# Units: m³/m³
KML_sm_range = 
# Export georeferenced KML file for another variable (yes/no):
make_KML_other = no
# Which variable/column?
KML_other_column = 
# Scale colors according to the range min/max:
KML_other_range = 
# Float format for all numbers. Example: x.yf (x is the full length and y the decimal precision)
KML_other_format = .1f
# Reverse color scale (yes/no):
KML_other_reverse = no

## Geojson
# Track polygons, e.g. for import in GIS systems (yes/no). Provides information on CRNS Footprint radius (in meter) and the representation of footprint geometry (point or polygons). EXPERIMENTAL: footprint_radius=50, footprint_type=polygon
make_footprint_polygons = no
