# %%
"""
This example shows how to apply spatial Kriging interpolation
and plot neat maps based on data from Corny output.
"""
import pandas
from corny.figures import Figure, make_bbox, Kriging
from corny.exdata.openstreetmap import OsmGraph

# %%
"""
Read data from Corny example output (created using rover.cfg).
"""
data = pandas.read_csv(
    "output/rover-2019082112.csv",
    index_col=0, parse_dates=True)

# Reduce to the relevant pieces
data = data[["utmx","utmy","lat","lon","moisture"]].dropna()

# %%
"""
Apply Kriging
"""
# Setup up Kriging instance
OKI = Kriging(data["lon"], data["lat"], data["moisture"],
            resolution=0.001,
            z_min=0.00, z_max=0.50,
            z_resolution=0.02)

# Run Kriging operator, creates OKI.z_grid
OKI.execute()

# Export as ESRI ASCII grid file
OKI.export_asc("output/sm_kriging_grid.asc")

# %%
"""
Make bounding box from data
"""
resolution = 0.001
bbox = make_bbox(
    data["lon"].values,
    data["lat"].values,
    pad=resolution)

# %%
"""
Download map features
"""
Rivers = OsmGraph("rivers", bbox).get()
Roads  = OsmGraph("roads",  bbox).get()

# %%
"""
Create the plot
"""
with Figure(
    size   = (9,9),
    layout = (2,1),
    projection = "flat",
    extent = bbox,
    abc=[
        "Kriging map of water content",
        "Quality of Kriging"
        ]
    ) as axes:

    ax = axes[0]
    from corny.figures import add_basemap
    add_basemap(
        ax=ax,
        extent=bbox,
        tiles="satellite-ms",
        zoom=14)

    # Plot interpolated soil moisture values
    OKI.plot_values(ax, contours_kw=dict(alpha=0.7))
    # Plot map features
    Rivers.plot(ax)
    Roads.plot(ax)

 
    ax = axes[1]
    # Plot variance map (quality of interpolation)
    OKI.plot_variance(ax)
    # Plot map features
    Rivers.plot(ax)
    Roads.plot(ax)

# %%
