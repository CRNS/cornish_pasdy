#!python 

from pathlib import Path
file = Path.home().joinpath('.kivy/garden/garden.matplotlib/backend_kivy.py')

print('Reading %s' % file, end='')

lines = []
finp = open(file, 'r') 
for line in finp:
    lines.append(line)
finp.close()

print(' (%d Bytes)' % file.stat().st_size)
print('Writing %s' % file, end='')

changed = 0
fout = open(file, 'w') 
for line in lines:
    if line.startswith('from matplotlib import _png'):
        line = '#' + line
        changed += 1
    fout.write(line)
fout.close()

print(' (%d Bytes)' % file.stat().st_size)